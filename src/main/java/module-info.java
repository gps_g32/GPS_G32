module pt.isec.gps_g32 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires annotations;
    requires java.base;

    opens pt.isec.gps_g32 to javafx.fxml;
    exports pt.isec.gps_g32;
    opens pt.isec.gps_g32.model.data.state to javafx.fxml;
    exports pt.isec.gps_g32.model.data.state;
    exports pt.isec.gps_g32.ui;
    opens pt.isec.gps_g32.ui to javafx.fxml;
    exports pt.isec.gps_g32.ui.controller;
    opens pt.isec.gps_g32.ui.controller to javafx.fxml;
    exports pt.isec.gps_g32.model;
    opens pt.isec.gps_g32.model to javafx.fxml;
    exports pt.isec.gps_g32.model.data;
    opens pt.isec.gps_g32.model.data to javafx.fxml;
    exports pt.isec.gps_g32.model.data.event;
    opens pt.isec.gps_g32.model.data.event to javafx.fxml;
    exports pt.isec.gps_g32.model.data.user;
    opens pt.isec.gps_g32.model.data.user to javafx.fxml;
}