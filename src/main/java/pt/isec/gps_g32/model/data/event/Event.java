package pt.isec.gps_g32.model.data.event;

import pt.isec.gps_g32.model.data.user.Interest;
import pt.isec.gps_g32.model.data.user.User;
import pt.isec.gps_g32.model.data.user.UserRatingEvent;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import static java.time.ZoneOffset.*;

public class Event {
    private static int newID = 0;
    private Integer id;
    private String name;
    private String type;
    private ArrayList<Interest> interests;
    private List<User>  participants;
    private String responsible;
    private boolean responsibleHasPermissions;
    private boolean visibility;
    private String description;
    private String local;
    private String hour;
    private String link;
    private String date;
    private String imagePath;
    private Double rating;
    private List<UserRatingEvent> ratingList;
    private Integer durationInMinutes;

    public Event(Integer id, String name, String type, String date, String hour, ArrayList<Interest> interests, String responsible, boolean responsibleHasPermissions, String description, boolean visibility, String local, String link, List<User> participants, String imagePath, Integer durationInMinutes, Double rating, List<UserRatingEvent> ratingList) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.date = date;
        this.hour = hour;
        this.interests = interests;
        this.responsible = responsible;
        this.responsibleHasPermissions = responsibleHasPermissions;
        this.description = description;
        this.visibility = visibility;
        this.local = local;
        this.link = link;
        this.participants = participants;
        if (this.participants == null) {
            this.participants = new ArrayList<>();
        }
        this.imagePath = imagePath;
        this.durationInMinutes = durationInMinutes;
        this.rating = rating;
        this.ratingList = ratingList;
        newID = id;
    }

    //Constructor que gera id
    public Event(String name, String type, String date, String hour, ArrayList<Interest> interests, String responsible, boolean responsibleHasPermissions, String description, boolean visibility, String local, String link, List<User> participants, String imagePath, Integer durationInMinutes, Double rating, List<UserRatingEvent> ratingList) {
        this.id = ++newID;
        this.name = name;
        this.type = type;
        this.date = date;
        this.hour = hour;
        this.interests = interests;
        this.responsible = responsible;
        this.responsibleHasPermissions = responsibleHasPermissions;
        this.description = description;
        this.visibility = visibility;
        this.local = local;
        this.link = link;
        this.participants = participants;
        if (this.participants == null) {
            this.participants = new ArrayList<>();
        }
        this.imagePath = imagePath;
        this.rating = rating;
        this.ratingList = ratingList;
        this.durationInMinutes = durationInMinutes;

    }

    public Boolean isLive() {
        DateTimeFormatter formatToUnixTimestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime eventStart = LocalDateTime.parse(getDate() + " " + getHour(), formatToUnixTimestamp);
        LocalDateTime currentTime = LocalDateTime.now();

        if (currentTime.isAfter(eventStart) && currentTime.isBefore(eventStart.plusMinutes(getDurationInMinutes()))){
            return true;
        }

        return false;
    }

    public Boolean isPast() {
        DateTimeFormatter formatToUnixTimestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime eventStart = LocalDateTime.parse(getDate() + " " + getHour(), formatToUnixTimestamp);
        LocalDateTime currentTime = LocalDateTime.now();

        if (currentTime.isAfter(eventStart.plusMinutes(getDurationInMinutes()))){
            return true;
        }

        return false;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Integer getId() {
        return id;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public String getResponsible() {
        return responsible;
    }

    public boolean isResponsibleHasPermissions() {
        return responsibleHasPermissions;
    }

    public String getDescription() {
        return description;
    }

    public String getLocal() {
        return local;
    }

    public String getHour() {
        return hour;
    }

    public String getLink() {
        return link;
    }

    public String getDate() {
        return date;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Double getRating() { return rating; }
    public Double getUserRating(String email) {
        for (UserRatingEvent userRatingEvent : this.ratingList) {
            if (userRatingEvent.getEmail().equals(email)) {
                return userRatingEvent.getRating();
            }
        }
        return null;
    }
    public List<UserRatingEvent> getRatingList() { return ratingList; }

    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    public Integer id(){
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public void setInterests(String interests) {
        this.interests.clear();
        String[] elementsArray = interests.split(";");
        for (String s : elementsArray) {
            this.interests.add(Interest.getInterestFromString(s));
        }
    }

    public void setResponsibleHasPermissions(Boolean responsibleHasPermissions) {
        this.responsibleHasPermissions = responsibleHasPermissions;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setLocal(String local) {
        this.local = local;
    }
    public void setRating(Double rating) { this.rating = rating; }

    public String getInterests() {
        StringBuilder sb = new StringBuilder();

        for (Interest s : this.interests) {
            if(!sb.isEmpty()) sb.append(";");
            sb.append(s);
        }
        return sb.toString();
    }

    public ArrayList<Interest> getInterests (boolean x){
        return this.interests;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void addInterest(Interest interest) {
        this.interests.add(interest);
    }

    //TODO [?]: adicionar o rating
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.id).append("_###_");
        sb.append(this.name).append("_###_");
        sb.append(this.type).append("_###_");
        sb.append(this.date).append("_###_");
        sb.append(this.hour).append("_###_");
        sb.append(this.durationInMinutes).append("_###_");

        for (Interest interest : this.interests) {
            sb.append(Interest.getId(interest)).append(";");
        }
        sb.append("_###_");
        sb.append(this.responsible).append("_###_");
        sb.append(this.responsibleHasPermissions).append("_###_");
        sb.append(this.description).append("_###_");
        sb.append(this.visibility).append("_###_");
        sb.append(this.local).append("_###_");
        sb.append(this.link).append("_###_");

        sb.append(getParticipantsString());
        sb.append("_###_");
        sb.append(this.imagePath);
        sb.append("_###_");
        sb.append(this.rating);
        sb.append(System.lineSeparator());

        return sb.toString();
    }

    public boolean isParticipant(User user) {
        if (participants == null) return false;
        return participants.contains(user);
    }

    public void removeParticipant(User loggedUser) {
        participants.remove(loggedUser);
    }

    public void addParticipant(User loggedUser) {
        participants.add(loggedUser);
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    public String getParticipantsString() {
        StringBuilder sb = new StringBuilder();

        if (this.participants == null)
            return "";

        if (!this.participants.isEmpty()) {
            for (User participant : this.participants) {
                if (!sb.isEmpty()) sb.append(";");
                sb.append(participant.getEmail());
            }
        }

        return sb.toString();
    }

    public void addRating(String email, Double rating) {
        UserRatingEvent userRatingEvent = new UserRatingEvent(email, this.id, rating);

        for(UserRatingEvent ure : this.ratingList){
            if(ure.getEmail().equals(email)){
                ure.setRating(rating);
                this.rating = calculateRating();
                return;
            }
        }

        this.ratingList.add(userRatingEvent);
        this.rating = calculateRating();
    }

    public Double calculateRating() {
        Double sum = 0.0;
        int size = this.ratingList.size();

        if (size > 0) {
            for (UserRatingEvent userRatingEvent : this.ratingList) {
                sum += userRatingEvent.getRating();
            }
            return sum / size;
        } else
            return 0.0;
    }
}
