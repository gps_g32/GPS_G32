package pt.isec.gps_g32.model.data.user;

public enum Interest {
    INFORMATICA (1),
    EGI(2),
    BIOENGENHARIA(3),
    MECANICA(4),
    CIVIL(5),
    ELETROMECANICA(6),
    ENGENHARIA(7),
    SOFTSKILLS(8),
    LICENCIATURAS(9),
    MESTRADOS(10),
    CTESP(11),
    ASSOCIATIVISMO(12),
    BIOENG(13), ENG(14), SOFT_SKILLS(15);


    final Integer id;

    Interest(Integer id){
        this.id = id;
    }

    static public Integer getId(Interest o){
        return o.id;
    }

    static public Interest getInterest(Integer o){
        switch (o) {
            case 1 -> {
                return INFORMATICA;
            }
            case 2 -> {
                return EGI;
            }
            case 3 -> {
                return BIOENGENHARIA;
            }
            case 4 -> {
                return MECANICA;
            }
            case 5 -> {
                return CIVIL;
            }
            case 6 -> {
                return ELETROMECANICA;
            }
            case 7 -> {
                return ENGENHARIA;
            }
            case 8 -> {
                return SOFTSKILLS;
            }
            case 9 -> {
                return LICENCIATURAS;
            }
            case 10 -> {
                return MESTRADOS;
            }
            case 11 -> {
                return CTESP;
            }
            case 12 -> {
                return ASSOCIATIVISMO;
            }case 13 -> {
                return BIOENG;
            } case 14 -> {
                return ENG;
            } case 15 -> {
                return SOFT_SKILLS;
            }
        }
        return null;
    }

    public static Interest getInterestFromString(String interestString) {
        for (Interest interest : values()) {
            if (interest.name().equalsIgnoreCase(interestString)) {
                return interest;
            }
        }

        return null;
    }

    @Override
    public String toString() {

        switch (this){

            case INFORMATICA -> {return "Informatica";}
            case EGI -> { return "EGI"; }
            case BIOENGENHARIA -> { return "Bioengenharia"; }
            case MECANICA -> { return "Mecanica"; }
            case CIVIL -> { return "Civil"; }
            case ELETROMECANICA -> { return "Eletromecanica"; }
            case ENGENHARIA -> { return "Engenharia"; }
            case SOFTSKILLS -> { return "SoftSkills"; }
            case LICENCIATURAS -> { return "Licenciaturas"; }
            case MESTRADOS -> { return "Mestrados"; }
            case CTESP -> { return "CTESP"; }
            case ASSOCIATIVISMO -> { return "Associativismo"; }

        }
        return "";
    }
}
