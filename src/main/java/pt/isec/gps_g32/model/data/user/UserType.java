package pt.isec.gps_g32.model.data.user;

public enum UserType {
    ADMIN("As an admin i can pretty much do anything!", 3),
    RESPONSIBLE_SUPER_USER("As a super user i can alter some definitions that the normal user cant!", 2),
    RESPONSIBLE_USER("As a user that is responsible for the management of the event i can alter some settings!", 1),
    USER("I am a user that wants to attend the events!", 0 );
    private final String description;
    private final Integer id;

    UserType(String description, Integer id){
        this.description = description;
        this.id = id;
    }

    static public Integer getId(UserType o){
        return o.id;
    }

    static public UserType getUserType(Integer o){
        switch (o) {
            case 0 -> {
                return USER;
            }
            case 1 -> {
                return RESPONSIBLE_USER;
            }
            case 2 -> {
                return RESPONSIBLE_SUPER_USER;
            }
            case 3 -> {
                return ADMIN;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "UserType{" +
                "description='" + description + '\'' +
                '}';
    }
}