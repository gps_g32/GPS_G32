package pt.isec.gps_g32.model.data.state;

public enum State {
    LOGIN("LOGIN"),
    MENU("Menu"),
    HISTORY("History of events"),
    ACCOUNT_INFO("Account information"),
    ACCOUNT_INFO_ADMIN("Admin Account information"),
    ADMIN("Admin create event"),
    CREATE_EVENT("Create event"),
    CREATE_EVENT_REQUEST("Create event request"),
    CHECK_EVENT("Check event"),
    EDIT_EVENT("Edit event"),
    EDIT_INTERESTS("Edit interests"),
    EVENT_INFO_USER("Event information - User"),
    ATTENDANCE("Attend event"),
    EVENT_LIST("List of events"),
    VIEW_EVENTS("View events - Admin"),
    CHANGE_PASS("Change password"),
    LIST_EVENT_REQUESTS("List Event Requests"),
    MENU_ADMIN("Menu for the admin"),
    CHOOSE("To choose what we want to see"),
    SELECT_USERS("Scroll through users"),
    SELECT_EVENTS("Scroll through events");

    private final String str;
    State(String str){
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
