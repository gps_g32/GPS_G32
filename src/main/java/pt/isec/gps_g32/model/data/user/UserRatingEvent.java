package pt.isec.gps_g32.model.data.user;

public class UserRatingEvent {
    private String email;
    private int eventID;
    private Double rating;

    public UserRatingEvent(String email, int eventID, Double rating) {
        this.email = email;
        this.eventID = eventID;
        this.rating = rating;
    }

    public String getEmail() { return email; }
    public int getEventID() { return eventID; }
    public Double getRating() { return rating; }

    public void setRating(Double rating) { this.rating = rating; }

    @Override
    public String toString() {
        return "UserRatingEvent{" +
                "email='" + email + '\'' +
                ", eventID=" + eventID +
                ", rating=" + rating +
                '}';
    }
}
