package pt.isec.gps_g32.model.data;

import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.Interest;
import pt.isec.gps_g32.model.data.user.User;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class FileManager {

    public static void writeToFile(String filename, Event event) {
        try {
            FileWriter fileWriter = new FileWriter(filename, true);

            String eventString = toFileString(event);
            fileWriter.write(eventString);

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToFile(String filename, User user) {
        try {
            FileWriter fileWriter = new FileWriter(filename);

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String toFileString(Event event) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(event.getId()).append("_###_");
        sb.append(event.getName()).append("_###_");
        sb.append(event.getType()).append("_###_");
        sb.append(event.getDate()).append("_###_");
        sb.append(event.getHour()).append("_###_");
        ArrayList<Interest> interests = event.getInterests(true);
        for (Interest interest : interests) {
            sb.append(Interest.getId(interest)).append(";");
        }
        sb.append("_###_");
        sb.append(event.getResponsible()).append("_###_");
        sb.append(event.isResponsibleHasPermissions()).append("_###_");
        sb.append(event.getDescription()).append("_###_");
        sb.append(event.getVisibility()).append("_###_");
        sb.append(event.getLocal()).append("_###_");
        sb.append(event.getLink()).append("_###_");
        sb.append("_###_");
        sb.append(event.getImagePath());
        sb.append(System.lineSeparator());

        return sb.toString();
    }

}
