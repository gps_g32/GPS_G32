package pt.isec.gps_g32.model.data.event;

import java.util.ArrayList;
import java.util.List;

public class EventData {

    private List<String> eventTypes;
    private List<String> eventLocations;
    private List<String> possibleTimes;
    private List<String> possibleDuration;

    public EventData() {

        this.eventTypes = new ArrayList<>();
        this.eventLocations = new ArrayList<>();
        this.possibleTimes = new ArrayList<>();
        this.possibleDuration = new ArrayList<>();

        eventTypes.add("Palestra");
        eventTypes.add("Seminário");
        eventTypes.add("Workshop");
        eventTypes.add("Conferência");
        eventTypes.add("Debate");

        eventLocations.add("A2.2 DEIS");
        eventLocations.add("ISEC");
        eventLocations.add("ESEC");

        for (int hours = 0; hours < 24; hours++) {
            for (int minutes = 0; minutes < 60; minutes += 30) {
                String formattedTime = String.format("%02d:%02d", hours, minutes);
                possibleTimes.add(formattedTime);
            }
        }

        for (int hours = 30; hours <= 120; hours+=30) {
            possibleDuration.add(String.valueOf(hours));
        }

    }

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public List<String> getEventLocations() {
        return eventLocations;
    }

    public List<String> getPossibleTimes() { return possibleTimes; }

    public List<String> getPossibleDuration() { return possibleDuration; }



    public void addEventType(String eventType) {
        eventTypes.add(eventType);
    }

    public void addEventLocation(String eventLocation) {
        eventLocations.add(eventLocation);
    }

    public void addPossibleTime(String possibleTime) {
        possibleTimes.add(possibleTime);
    }

    //public void addPossibleDuration(String possibleDuration) { possibleDuration.add(possibleDuration); }
}
