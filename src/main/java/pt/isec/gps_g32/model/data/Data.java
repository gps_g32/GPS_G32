package pt.isec.gps_g32.model.data;

import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.*;
import pt.isec.gps_g32.model.data.state.State;

import java.io.*;
import java.util.*;

public class Data {
    //We define the path of the Datafiles here
    public static final String PATH_TO_USERS = "./DataFiles/users.txt";
    public static final String PATH_TO_USERS_CODE = "./DataFiles/usersCode.txt";
    public static final String PATH_TO_USERS_RATING = "./DataFiles/usersRating.txt";
    public static final String PATH_TO_EVENTS = "./DataFiles/events.txt";
    private static final String PATH_TO_REQUESTS = "./DataFiles/requests.txt";

    int activeEvent;
    HashMap<Integer, Event> events;
    HashMap<String, User> users;
    HashMap<Integer, Event> eventRequests;
    HashMap<String, UserCode> userCodes;
    HashMap<Integer, UserRatingEvent> userRatingEvents;
    User loggedUser;
    Filter filter;

    State state;

    public Data () {
        this.events = new HashMap<>();
        this.users = new HashMap<>();
        this.eventRequests = new HashMap<>();
        this.userCodes = new HashMap<>();
        this.userRatingEvents = new HashMap<>();
        this.state = State.LOGIN;
        this.filter = Filter.INTERESTED;
        _loadUsersRating();
        _loadEvents();
        _loadUsers();
        _loadParticipations(); //Since users are loaded after events are, we need to load participations into events after loading users
        _loadEventRequests();
        _loadUsersCodes();
    }

    public void saveData() {
        saveUsers();
        saveEvents();
        saveUserRating();

        try(FileWriter fileWriter = new  FileWriter(PATH_TO_REQUESTS);){
            for(Event event : eventRequests.values())
                fileWriter.write(event.toString());
        }catch (Exception ignore){}
    }

    private void _loadEvents(){
        try( FileReader fr = new FileReader(PATH_TO_EVENTS)){
            Scanner sc = new Scanner(fr);
            while(sc.hasNext()){
                String line = sc.nextLine();
                String[] event = line.split("_###_");
                ArrayList<Interest> interests = new ArrayList<>();

                String id = event[0];
                String name = event[1];
                String type = event[2];
                String date = event[3];
                String hour = event[4];
                String duration = event[5];

                try {
                    String listOfInterestsinString = event[6];
                    for(String interest : listOfInterestsinString.split(";")){
                        Integer integer = Integer.parseInt(interest);
                        interests.add(Interest.getInterest(integer));
                    }
                } catch (Exception ignored) {}

                String responsibleEmail  = event[7];
                String responsibleHasPermissions = event[8];
                String description = event[9];
                String visibility = event[10];
                String place = event[11];
                String link = event[12];
                //participants are loaded after users are loaded
                String imagePath = event[14];
                String rating = event[15];

                Event e = new Event(Integer.parseInt(id), name,type, date, hour, interests, responsibleEmail, Boolean.parseBoolean(responsibleHasPermissions), description, Boolean.parseBoolean(visibility), place, link, new ArrayList<>(), imagePath, Integer.parseInt(duration), Double.parseDouble(rating), getUserRatingEvents(Integer.parseInt(id)));
                this.events.put(e.getId(), e);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //TODO: javaDoc e carregar historico de eventos
    //TODO: limpar o codigo

    private void _loadUsers() {
        try ( FileReader fr = new FileReader(PATH_TO_USERS);){
            Scanner sc = new Scanner(fr);
            while(sc.hasNext()){
                String line = sc.nextLine();
                String[] values = line.split("_###_");

                String type = values[0];
                UserType typeEnum = UserType.getUserType(Integer.valueOf(type));

                String email = values[1];
                String password = values[2];
                String name = values[3];
                String interestsString = null;
                String favouritesString = null;
                String eventHistoryString = null;

                //TODO: parse eventHistory
                ArrayList<Event> eventHistoryList = new ArrayList<>();
                ArrayList<Event> favourites = new ArrayList<>();
                ArrayList<Interest> interests = new ArrayList<>();

                try {
                    interestsString = values[4];
                    for(String interest : interestsString.split(";")){
                        if (interest.isEmpty()) break;
                        Integer integer = Integer.parseInt(interest);
                        interests.add(Interest.getInterest(integer));
                    }
                } catch (Exception ignored) {}

                try {
                    favouritesString = values[5];
                    for(String favourite: favouritesString.split(";")){
                        if (favourite.isEmpty()) break;
                        Integer eventId = Integer.parseInt(favourite);
                        Event event = events.get(eventId);
                        favourites.add(event);
                    }
                } catch (Exception ignored) {}

                try {
                    eventHistoryString = values[6];
                    for(String event: eventHistoryString.split(";")){
                        if (event.isEmpty()) break;
                        Integer eventId = Integer.parseInt(event);
                        Event eventTemp = events.get(eventId);
                        eventHistoryList.add(eventTemp);
                    }
                } catch (Exception ignored) {}

                User u = new User(email, password, name, eventHistoryList, favourites, interests, typeEnum);
                this.users.put(u.getEmail(),u);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void _loadEventRequests() {
        try( FileReader fr = new FileReader(PATH_TO_REQUESTS)){
            Scanner sc = new Scanner(fr);
            while(sc.hasNext()){
                String line = sc.nextLine();
                String[] event = line.split("_###_");

                String id = event[0];
                String name = event[1];
                String type = event[2];
                String date = event[3];
                String hour = event[4];
                String duration = event[5];

                String listOfInterestsinString = event[6];
                ArrayList<Interest> interests = new ArrayList<>();
                for(String interest : listOfInterestsinString.split(";")){
                    Integer integer = Integer.parseInt(interest);
                    interests.add(Interest.getInterest(integer));
                }

                String responsibleEmail  = event[7];
                String responsibleHasPermissions = event[8];
                String description = event[9];
                String visibility = event[10];
                String place = event[11];
                String link = event[12];
                //participants stay null for now
                String imagePath = event[13];

                Event e = new Event(Integer.parseInt(id), name,type, date, hour, interests, responsibleEmail, Boolean.parseBoolean(responsibleHasPermissions), description, Boolean.parseBoolean(visibility), place, link, null, imagePath, Integer.parseInt(duration), 0.0, getUserRatingEvents(Integer.parseInt(id)));
                this.eventRequests.put(e.getId(), e);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private void _loadUsersCodes() {
        try ( FileReader fr = new FileReader(PATH_TO_USERS_CODE);) {
            Scanner sc = new Scanner(fr);
            while (sc.hasNext()) {
                String line = sc.nextLine();
                String[] values = line.split("_###_");
                String email = values[0];
                String code = values[1];
                UserCode uc = new UserCode(email, code);
                this.userCodes.put(uc.getEmail(), uc);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void _loadUsersRating() {
        try ( FileReader fr = new FileReader(PATH_TO_USERS_RATING);) {
            Scanner sc = new Scanner(fr);
            while (sc.hasNext()) {
                String line = sc.nextLine();
                String[] values = line.split("_###_");
                String email = values[0];
                String eventID = values[1];
                String rating = values[2];
                UserRatingEvent ure = new UserRatingEvent(email, Integer.parseInt(eventID), Double.parseDouble(rating));
                this.userRatingEvents.put(ure.getEventID(), ure);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void _loadParticipations() {
        try( FileReader fr = new FileReader(PATH_TO_EVENTS)){
            Scanner sc = new Scanner(fr);
            while(sc.hasNext()){
                String line = sc.nextLine();
                String[] event = line.split("_###_");
                List<User> participants = new ArrayList<>();

                String id = event[0];

                try {
                    String listOfParticipants = event[13];
                    for(String participant : listOfParticipants.split(";")){
                        if (participant.isEmpty()) break;
                        User user = users.get(participant);
                        participants.add(user);
                    }
                    Event e = events.get(Integer.parseInt(id));
                    e.setParticipants(participants);
                } catch (Exception ignored) {}
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    // Getter and Setter
    public State getState(){
        return this.state;
    }
    public Set<Event> getEvents() {
        return new HashSet<>(events.values());
    }

    //TODO: get OcurringEvents - percorrer hashset de eventos para verificar quais os que estao live


    //TODO:
    public List<Event> getEventHistoryOfUser(String email){
       return this.users.get(email).getEventHistory();
    }

    //TODO:
    public List<Event> getFavoritesOfUser(String email){
        List<Event> ret = new ArrayList<>();
        for (String s : this.users.get(email).getFavouritesString().split(";")) {
            if (s.isEmpty())
                break;
            ret.add(events.get(Integer.valueOf(s)));
        }
        return ret;
    }

    //TODO:
    public List<User> getUsersThatWentToEvent(Integer id) {
        return events.get(id).getParticipants();
    }

    //TODO:
    public List<User> getUsersThatHaveEventFavorite(Integer id) {
        List<User> usersThatHaveTheEventFavorite = new ArrayList<>();

        for (User user : users.values()) {
            if (user.getFavouritesString().contains(id.toString())) {
                usersThatHaveTheEventFavorite.add(user);
            }
        }

        return usersThatHaveTheEventFavorite;
    }

    public Set<Event> getEventRquests() {
        System.out.println(eventRequests);
        return new HashSet<>(eventRequests.values());
    }

    public Set<Event> getLoggerEvents() {
        if(loggedUser == null)
            return null;

        Set<Event> events = new HashSet<>();
        switch (this.filter){
            case INTERESTED -> {
                ArrayList<Interest> interests = loggedUser.getInterests();
                for (Integer eventId: this.events.keySet()){
                    Event e = this.events.get(eventId);
                    for (Interest interest : e.getInterests(true)) {
                        for (Interest interest1 : interests) {
                            if(interest1.compareTo(interest) == 0) {
                                events.add(e);
                            }
                        }
                    }
                }
            }

            case NOT_INTERESTED -> {
                for (Integer eventId: this.events.keySet()){
                    Event e = this.events.get(eventId);
                    events.add(e);
                }
                ArrayList<Interest> interests = loggedUser.getInterests();
                for (Integer eventId: this.events.keySet()){
                    Event e = this.events.get(eventId);
                    for (Interest interest : e.getInterests(true)) {
                        for (Interest interest1 : interests) {
                            if(interest1.compareTo(interest) == 0){
                                events.remove(e);
                            }
                        }
                    }
                }
            }
            case ALL_EVENTS -> {
                for (Integer eventId: this.events.keySet()){
                    Event e = this.events.get(eventId);
                    events.add(e);
                }
                return events;
            }
        }

        return events;
    }
    
    public List<Event> getEventHistory(){
        if(loggedUser == null) return null;

        return loggedUser.getEventHistory();
    }

    /**
     * Get users set.
     *
     * @return the set
     */
    public Set<User> getUsers(){
        Set<User> users = new HashSet<>();
        for(User tempEvent: this.users.values()){
            users.add(tempEvent);
        }
        return users;
    }



    /**
     * Get user pass string.
     *
     * @param email the email
     * @return the string
     */
    public String getUserPass(String email){
        return users.get(email).getPassword();
    }

    public Event getActiveEvent() {
        System.out.println(activeEvent + " - " + events.get(activeEvent));
        return events.get(activeEvent);
    }

    public User getUser() {
        return loggedUser;
    }

    public UserType getUserType() {
        return loggedUser.getUserType();
    }

    /*public User getUser(String email) {
        for (User u: users.values()) {
            if (u.getEmail().equals(email))
                return u;
        }
        return null;
    }

    public User getLoggedUser() {
        return loggedUser;
    }*/



    public void setCurrentFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * Get user code.
     *
     * @param email the email
     * @return the user code
     */
    public UserCode getUserCode(String email){
        for (UserCode uc: userCodes.values()) {
            if (uc.getEmail().equals(email))
                return uc;
        }
        return null;
    }

    public User getUser(String email) {
        for (User u: users.values()) {
            if (u.getEmail().equals(email))
                return u;
        }
        return null;
    }

    public User getLoggedUser() {
        //System.out.println(loggedUser.getUserType().toString());
        return loggedUser;
    }

    public void setState(State newState){
        this.state = newState;
    }

    // Funtions

    /**
     * Set logger user.
     *
     * @param email the email
     */
    public void setLoggerUser(String email){
        System.out.println("O EMAIL PARA O UTILIZADOR LOGGED É" + email);
        loggedUser = users.get(email);
        System.out.println(loggedUser.getUserType());
    }

    public boolean createEvent(Event event){
        if (loggedUser == null || loggedUser.getUserType() != UserType.ADMIN || event == null)
            return false;

        if (!checkEventFields(event))
            return false;

        events.put(event.getId(), event);
        return true;
    }

    // Funtions
    /**
     * Authenticate boolean.
     *
     * @param email    the email
     * @param password the password
     * @return the boolean
     */
    public boolean authenticate(String email, String password){
        if (!validateEmail(email))
            return false;
        if(users.get(email).getPassword().equals(password))
            setLoggerUser(email);
        else
            return false;

        return true;
    }

    /**
     * Validate email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public boolean validateEmail(String email){ return users.get(email) != null; }

    /**
     * Create user code.
     *
     * @param email the email
     */
    public void createUserCode(String email){
        // Create a new UserCode object
        if (getUserCode(email) != null || !validateEmail(email))
            return;

        UserCode newUserCode = new UserCode(email);
        String newCode = newUserCode.getCode();
        String newLine = email + "_###_" + newCode;

        // Read and update the file
        List<String> lines = new ArrayList<>();
        boolean emailFound = false;

        try (BufferedReader br = new BufferedReader(new FileReader(PATH_TO_USERS_CODE))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith(email + "_###_")) {
                    // If email is found, update the line with the new code
                    lines.add(newLine);
                    emailFound = true;
                } else {
                    lines.add(line);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Error reading userCodes.txt: " + e.getMessage());
        }

        // If the email is not found, add a new line
        if (!emailFound) {
            lines.add(newLine);
        }

        // Write the updated content back to the file
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(PATH_TO_USERS_CODE))) {
            for (String line : lines) {
                bw.write(line);
                bw.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("Error writing userCodes.txt: " + e.getMessage());
        }

        // Update the UserCode set
        userCodes.remove(email);
        userCodes.put(email, newUserCode);
        System.out.println(userCodes);
    }

    public boolean validateCode(String email, String code){
        if (getUserCode(email) == null)
            return false;

        return getUserCode(email).getCode().equals(code);
    }

    /**
     * Change password.
     *
     * @param newPassword the new password
     */
    public void changePassword(String newPassword){
        if (loggedUser == null)
            return;

        loggedUser.setPassword(newPassword);
        saveUsers();
    }

    public void clickedEvent(int eventID) {
        this.activeEvent = eventID;
    }

    public boolean saveEvents() {
        /*try (FileWriter writer = new FileWriter(PATH_TO_EVENTS)) {
            for (Integer intId  : events.keySet()) {
                Event e = events.get(intId);
                writer.write(e.getId() + "_###_" + e.getName() + "_###_" + e.getType() + "_###_" + e.getDate() + "_###_" + e.getHour() + "_###_" + e.getDurationInMinutes() + "_###_");
                for (Interest interest : e.getInterests(true)) {
                    writer.write(Interest.getId(interest) + ";");
                }
                writer.write("_###_" + e.getResponsible() + "_###_" + e.isResponsibleHasPermissions() + "_###_" + e.getDescription() + "_###_" + e.getVisibility() + "_###_" + e.getLocal() + "_###_" + e.getLink() + "_###_" + e.getParticipantsString() + "_###_" + e.getImagePath() + "_###_" + e.getRating() + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }*/

        try(FileWriter fileWriter = new  FileWriter(PATH_TO_EVENTS);){
            for(Event event : events.values())
                fileWriter.write(event.toString());
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean checkEventFields(Event event) {

        if (event.getName() == null || event.getName().isEmpty()) return false;

        if (event.getType() == null || event.getType().isEmpty()) return false;

        if (event.getDate() == null || event.getDate().isEmpty()) return false;

        if (event.getHour() == null || event.getHour().isEmpty()) return false;

        if (event.getDurationInMinutes() == null || event.getDurationInMinutes() == 0) return false;

        if (event.isResponsibleHasPermissions() && (event.getResponsible() == null || event.getResponsible().isEmpty())) return false;

        if (event.getLocal() == null || event.getLocal().isEmpty()) return false;

        if (event.getVisibility() == null) return false;

        return true;
    }

    public boolean deleteEvent() {
        if (loggedUser == null || loggedUser.getUserType() != UserType.ADMIN)
            return false;

        if(getActiveEvent() == null)
            return false;

        events.remove(activeEvent);
        activeEvent = -1;
        return saveEvents();
    }

    public void createEventsFile() {
        File file = new File("./DataFiles/events.txt");

        String content = "1_###_Evento numero 1_###_Palestra_###_2023-10-15_###_15:30_###_1;2;3;5;_###_tiago@isec.pt_###_true_###_Evento muito bom! Tens de vir!!!!_###_true_###_ISEC_###_google.pt_###__###_evento1.png\n" +
                "2_###_Fazer o trabalho de GPS_###_Workshop_###_2023-10-16_###_16:30_###_1;2;6;7;_###_tigasFigs@isec.pt_###_false_###_Evento muito mau! Nao venhas!!!!_###_false_###_ESEC_###__###__###_evento2.png\n" +
                "3_###_Fazer o trabalho de GPS2_###_Palestra_###_2023-10-17_###_15:30_###_1;_###_tiago@isec.pt_###_true_###_Evento muito bom! Tens de vir!!!!_###_true_###_ISEC_###__###__###_evento3.png";
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

        events = new HashMap<>();
        _loadEvents();
    }

    public boolean getLoggedUserIsAdmin() {
        if (loggedUser == null)
            return false;
        return this.loggedUser.getUserType() == UserType.ADMIN;
    }

    public boolean createEventRequest(Event event) {
        if (loggedUser == null || getLoggedUserIsAdmin())
            return false;

        if (checkEventFields(event)) {
            event.setResponsible(loggedUser.getEmail());
            eventRequests.put(event.getId(), event);
            return true;
        }

        return false;
    }
    public void saveUserInterests(ArrayList<Interest> interests) {
        this.loggedUser.setInterests(interests);
        System.out.println(Arrays.toString(this.loggedUser.getInterests().toArray()));

        for (User user:
             users.values()) {
            System.out.println(user.toString());
        }
    }

    public boolean isFavourite(int eventID) {
        if (loggedUser == null)
            return false;

        return loggedUser.isFavourite(events.get(eventID));
    }

    public void removeFavourite(int eventID) {
        if (loggedUser == null)
            return;

        loggedUser.removeFavourite(events.get(eventID));
    }

    public void addFavourite(int eventID) {
        if (loggedUser == null)
            return;

        loggedUser.addFavourite(events.get(eventID));
    }

    public void saveUsers() {
        try (FileWriter writer = new FileWriter(PATH_TO_USERS)) {
            for (String email : users.keySet()) {
                User u = users.get(email);
                writer.write(UserType.getId(u.getUserType()) + "_###_" + u.getEmail() + "_###_" + u.getPassword() + "_###_" + u.getName() + "_###_" + u.getInterestsString() + "_###_" + u.getFavouritesString() + "_###_" + u.getEventHistoryString() + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean getFavourite(int eventID) {
        if (loggedUser == null)
            return false;

        return loggedUser.isFavourite(events.get(eventID));
    }

    public List<Integer> getEventsForComboBox() {
        return this.events.keySet().stream().toList();
    }

    public List<String> getUsersForComboBox() {
        return this.users.keySet().stream().toList();
    }

    public boolean processEventRequest(int eventID, boolean requestAnswer, boolean hasPermissions) {
        if (loggedUser == null || !getLoggedUserIsAdmin() || !eventRequests.containsKey(eventID))
            return false;

        if (requestAnswer) {
            events.put(eventID, eventRequests.get(eventID));
            if (hasPermissions)
                events.get(eventID).setResponsibleHasPermissions(hasPermissions);
        }
        eventRequests.remove(eventID);
        return true;
    }

    public void createUser(String mail) {
        if(users.containsKey(mail)) return;
        User u = new User(mail, "123", "User", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), UserType.USER);
        users.put(u.getEmail(), u);
    }

    public boolean isParticipating(int eventID) {
        if (loggedUser == null)
            return false;

        Event event = events.get(eventID);
        return event != null && event.isParticipant(loggedUser);
    }

    public void removeParticipation(int eventID) {
        if (loggedUser == null)
            return;

        Event event = events.get(eventID);
        if (event != null) {
            event.removeParticipant(loggedUser);
            loggedUser.removePresence(event);
        }
    }

    public void addParticipation(int eventID) {
        if (loggedUser == null)
            return;

        Event event = events.get(eventID);
        if (event != null) {
            event.addParticipant(loggedUser);
            loggedUser.addPresence(event);
        }
    }

    public boolean checkEventExists(int i) {
        return events.containsKey(i);
    }

    private List<UserRatingEvent> getUserRatingEvents(int eventID) {
        List<UserRatingEvent> ret = new ArrayList<>();
        for (UserRatingEvent ure : userRatingEvents.values()) {
            if (ure.getEventID() == eventID)
                ret.add(ure);
        }
        return ret;
    }

    public void addUserRating(Double rating) {
        UserRatingEvent userRatingEvent = new UserRatingEvent(loggedUser.getEmail(), this.activeEvent, rating);

        for(UserRatingEvent ure : this.userRatingEvents.values()){
            if(ure.getEmail().equals(loggedUser.getEmail())){
                ure.setRating(rating);
                this.events.get(activeEvent).addRating(loggedUser.getEmail(), rating);
                return;
            }
        }

        this.userRatingEvents.put(userRatingEvent.getEventID(), userRatingEvent);
        this.events.get(activeEvent).addRating(loggedUser.getEmail(), rating);
    }

    public void saveUserRating() {
        try (FileWriter writer = new FileWriter(PATH_TO_USERS_RATING)) {
            for (UserRatingEvent ure : userRatingEvents.values()) {
                writer.write(ure.getEmail() + "_###_" + ure.getEventID() + "_###_" + ure.getRating() + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}