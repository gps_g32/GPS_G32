package pt.isec.gps_g32.model.data.user;

public enum Filter {
    ALL_EVENTS, INTERESTED, NOT_INTERESTED;
}
