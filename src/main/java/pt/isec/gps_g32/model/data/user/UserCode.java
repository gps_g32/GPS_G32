package pt.isec.gps_g32.model.data.user;

import java.util.Random;

public class UserCode {
    private String email;
    private String code;
    private int length = 6;

    public UserCode(String email) {
        this.email = email;
        this.code = generateCode();
    }

    public UserCode(String email, String code) {
        this.email = email;
        this.code = generateCode();
    }

    public String getEmail() {
        return email;
    }

    public String getCode() {
        return code;
    }

    private String generateCode() {
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder codeBuilder = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            codeBuilder.append(characters.charAt(index));
        }

        this.code = codeBuilder.toString();
        return code;
    }

    @Override
    public String toString() {
        return "UserCode {" +
                "email='" + email + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
