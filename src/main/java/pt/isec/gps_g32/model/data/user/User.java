package pt.isec.gps_g32.model.data.user;

import pt.isec.gps_g32.model.data.event.Event;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String email;
    private String password;
    private String name;
    private List<Event> eventHistory;
    private List<Event> favourites;
    private ArrayList<Interest> interests;
    private UserType userType;

    public User(String email, String password, String name, List<Event> eventHistory, List<Event> favourites, ArrayList<Interest> interests, UserType type) {
        this.email = email;
        this.password = password;
        this.name = name;

        if(favourites != null)
            this.favourites = favourites;
        else
            this.favourites = new ArrayList<>();

        if (eventHistory != null)
            this.eventHistory = eventHistory;
        else
            eventHistory = new ArrayList<>();

        this.interests = interests;
        this.userType = type;
    }

    public boolean addPresence(Event event) {
        return eventHistory.add(event);
    }

    public boolean removePresence(Event event) {
        return eventHistory.remove(event);
    }

    //getters and setters
    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }
    public String getName() {
        return name;
    }
    public ArrayList<Interest> getInterests(){
        return this.interests;
    }
    public String getInterestsString() {
        StringBuilder sb = new StringBuilder();

        for (Interest s : this.interests) {
            if(!sb.isEmpty()) sb.append(";");
            sb.append(s.id);
        }
        return sb.toString();
    }
    public List<Event> getEventHistory() {
        return eventHistory;
    }
    public UserType getUserType() {
        return userType;
    }
    public void setUserType(UserType userType) {
        this.userType = userType;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns a string representation of the User object, including its email, password, name,
     * and event history.
     *
     * @return A string representation of the User object.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.email).append("_###_");
        sb.append(this.password).append("_###_");
        sb.append(this.name).append("_###_");
        sb.append(getInterestsString());
        sb.append("_###_");
        sb.append(getFavouritesString());
        sb.append("_###_");
        sb.append(getEventHistoryString());
        sb.append("_###_");
        sb.append(UserType.getId(this.getUserType())).append(System.lineSeparator());
        return sb.toString();
    }

    public void setInterests(ArrayList<Interest> interests) {
        this.interests = interests;
    }

    public boolean isFavourite(Event event) {
        return favourites.contains(event);
    }

    public List<Event> getFavourites() {
        return favourites;
    }

    public void removeFavourite(Event event) {
        favourites.remove(event);
    }

    public void addFavourite(Event event) {
        favourites.add(event);
    }


    public String getFavouritesString() {
        if (this.favourites == null) return "";
        StringBuilder sb = new StringBuilder();

        for (Event s : this.favourites) {
            if(!sb.isEmpty()) sb.append(";");
            sb.append(s.getId());
        }
        return sb.toString();
    }

    public String getEventHistoryString() {
        if (this.eventHistory == null) return "";
        StringBuilder sb = new StringBuilder();

        for (Event s : this.eventHistory) {
            if(!sb.isEmpty()) sb.append(";");
            sb.append(s.getId());
        }
        return sb.toString();
    }
}