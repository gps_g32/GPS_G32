package pt.isec.gps_g32.model;

import pt.isec.gps_g32.model.data.Data;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.Filter;
import pt.isec.gps_g32.model.data.user.Interest;
import pt.isec.gps_g32.model.data.user.User;
import pt.isec.gps_g32.model.data.user.UserCode;
import pt.isec.gps_g32.model.data.state.State;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

public class ModelManager {

    Data context;
    PropertyChangeSupport pcs;

    public static final String PROP_STATE = "state";
    public static final  String PROP_DATA = "data";

    public ModelManager(){
        //set state to log in
        pcs = new PropertyChangeSupport(this);
        this.context = new Data();
    }

    //Function to change the current state

    //Function to get the current state
    public State getState() {
        return context.getState();
    }

    public void changeState(State newState) {
        context.setState(newState);
        pcs.firePropertyChange(PROP_STATE, null, null);
    }


    public List<User> getUsersThatWentToEvent(Integer id) {
        return context.getUsersThatWentToEvent(id);
    }


    public List<User> getUsersThatHaveEventFavorite(Integer id){
        return context.getUsersThatHaveEventFavorite(id);
    }

    //Function to add a listener to the property change support
    public boolean addPropertyChangeListener(String property, PropertyChangeListener listener){
        pcs.addPropertyChangeListener(property, listener );
        return true;
    }

    public void goToCreate() {
        if (context.getLoggedUserIsAdmin())
            changeState(State.CREATE_EVENT);
        else
            changeState(State.CREATE_EVENT_REQUEST);
    }

    public boolean createEvent(Event event){
        if (!context.createEvent(event))
            return false;
        pcs.firePropertyChange(PROP_DATA, null, null);
        return true;
    }

    public List<Event> getEventHistory() {
        return context.getEventHistory();
    }

    public Set<Event> getEvents(){
        return context.getLoggerEvents();
    }

    /**
     * Authenticate boolean.
     *
     * @param email    the email
     * @param password the password
     * @return the boolean
     */
    public boolean authenticate(String email, String password){
        return context.authenticate(email, password);
    }

    /**
     * Validate email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public boolean validateEmail(String email){
        return context.validateEmail(email);
    }

    /**
     * Create user code.
     *
     * @param email the email
     */
    public void createUserCode(String email){
        context.createUserCode(email);
    }

    /**
     * Get user code.
     *
     * @param email the email
     * @return the user code
     */
    public UserCode getUserCode(String email){
        return context.getUserCode(email);
    }

    /**
     * Validate code boolean.
     *
     * @param email the email
     * @param code  the code
     * @return the boolean
     */
    public boolean validateCode(String email, String code) {
        return context.validateCode(email, code);
    }

    /**
     * Set logger user.
     *
     * @param email the email
     */
    public void setLoggerUser(String email){
        context.setLoggerUser(email);
    }

    /**
     * Change password.
     *
     * @param password the password
     */
    public void changePassword(String password){
        context.changePassword(password);
    }

    public User getUser(String email){
        return context.getUser(email);
    }
    public User getLoggedUser(){
        return context.getLoggedUser();
    }

    public void clickedEvent(int eventID) {
        context.clickedEvent(eventID);
    }

    public Event getActiveEvent() {
        return context.getActiveEvent();
    }

    public boolean saveEvents() {
        pcs.firePropertyChange(PROP_DATA, null, null);
        return context.saveEvents();
    }

    /*
    public Object getUser() {
        return context.getUser();
    }

    public UserType getUserType() {
        return context.getUserType();
    }
    */

    public Set<User> getUsers() {
        return context.getUsers();
    }

    public boolean deleteEvent() {
        pcs.firePropertyChange(PROP_DATA, null, null);
        return context.deleteEvent();
    }

    public void setCurrentEventFilter(Filter filter) {
        context.setCurrentFilter(filter);
        this.pcs.firePropertyChange(PROP_DATA, null, null);
    }

    public void saveUserInterests(ArrayList<Interest> interests){
        context.saveUserInterests(interests);
    }

    public boolean getLoggedIsAdmin() {
        return context.getLoggedUserIsAdmin();
    }


    public boolean createEventRequest(Event event) {
        return context.createEventRequest(event);
    }

    public void save() {
        context.saveData();
    }

    public void saveUsers() {
        pcs.firePropertyChange(PROP_DATA, null, null);
        context.saveUsers();
    }

    //public void createEventsFile() { context.createEventsFile(); }

    public boolean isFavourite(int eventID) {
        return context.isFavourite(eventID);
    }

    public void removeFavourite(int eventID) {
        context.removeFavourite(eventID);
        this.pcs.firePropertyChange(PROP_DATA, null, null);
    }

    public void addFavourite(int eventID) {
        context.addFavourite(eventID);
        this.pcs.firePropertyChange(PROP_DATA, null, null);
    }

    public boolean getFavourite(int eventID) {
        return context.getFavourite(eventID);
    }

    public Set<Event> getEventRequests() {
        return context.getEventRquests();

    }

    public boolean processEventRequest(int eventID, boolean requestAnswer, boolean hasPermitions) {
        var ret = context.processEventRequest(eventID, requestAnswer, hasPermitions);
        this.pcs.firePropertyChange(PROP_DATA, null, null);
        return ret;
    }

    public void createUser(String mail) {
        context.createUser(mail);
    }

    public boolean isParticipating(int eventID) {
        return context.isParticipating(eventID);
    }

    public void removeParticipation(int eventID) {
        context.removeParticipation(eventID);
        this.pcs.firePropertyChange(PROP_DATA, null, null);
    }

    public void addParticipation(int eventID) {
        context.addParticipation(eventID);
        this.pcs.firePropertyChange(PROP_DATA, null, null);
    }

    public boolean checkEventExists(int i) {
        return context.checkEventExists(i);
    }


    public List<Integer> getEventsForComboBox() {
        return context.getEventsForComboBox();
    }

    public List<String> getUsersForComboBox() {
        return context.getUsersForComboBox();
    }

    public List<Event> getEventsThatUserHasGoneTo(String email){
        return context.getEventHistoryOfUser(email);
    }

    public List<Event> getFavoritesOfUser(String email){
        return context.getFavoritesOfUser(email);
    }

    public void addUserRating(double rating) {
        context.addUserRating(rating);
    }
}

