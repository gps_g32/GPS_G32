package pt.isec.gps_g32;

import javafx.application.Application;
import pt.isec.gps_g32.ui.IPC4You;

public class Main {
    public static void main(String[] args) {
        Application.launch(IPC4You.class, args);
    }
}
