package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.state.State;
import pt.isec.gps_g32.model.data.user.User;
import pt.isec.gps_g32.ui.IPC4You;

import java.util.List;

public class SelectUsersController extends Controller       {
    //Temporary

    @FXML public StackPane stackPaneMenus;
    @FXML public ComboBox usersList;
    @FXML public AnchorPane apParaEventosFavoritos;
    @FXML public AnchorPane apParaAsPresencas;
    @FXML HBox hBoxParaAsPresencas;
    @FXML HBox hBoxParaEventosFavoritos;


    public void setModel(ModelManager model) {
        this.model = model;

        hBoxParaEventosFavoritos.getChildren().clear();
        hBoxParaAsPresencas.getChildren().clear();
    }

    public void registerHandlers() {

        this.usersList.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue!= null) {
                        System.out.println("A ir buscar os utilizadores do evento" + newValue);
                        _changeContent(newValue.toString());
                    }
                }
        );

        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        update();
    }

    private void _changeContent(String newValue) {
        List<Event> usersThatHaveEventFavorite = model.getEventsThatUserHasGoneTo(newValue);
        List<Event> usersThatWentToTheEvent = model.getFavoritesOfUser(newValue);

        hBoxParaEventosFavoritos.getChildren().clear();
        hBoxParaAsPresencas.getChildren().clear();

        _setPresences(usersThatWentToTheEvent);
        _setFavorites(usersThatHaveEventFavorite);
    }

    private void _setFavorites(List<Event> usersThatHaveEventFavorite) {
        for (Event e : usersThatHaveEventFavorite) {
            Parent child = null;
            FXMLLoader fxmlLoader;
            fxmlLoader = new FXMLLoader(IPC4You.class.getResource("event-preview-view.fxml"));
            try {
                child = fxmlLoader.load();
            } catch (Exception ignore) {
            }
            EventPreviewController lvc = fxmlLoader.getController();
            lvc.setModel(model, e.getId());
            lvc.build(e);
            hBoxParaEventosFavoritos.getChildren().add(child);
        }

        apParaEventosFavoritos.setPrefWidth(350 * (usersThatHaveEventFavorite.size() + 1));
        hBoxParaEventosFavoritos.setPrefWidth(350 * (usersThatHaveEventFavorite.size() + 1));
    }
    private void _setPresences(List<Event> usersThatWentToTheEvent) {
        for (Event e: usersThatWentToTheEvent) {
            Parent child = null;
            FXMLLoader fxmlLoader;
            fxmlLoader = new FXMLLoader(IPC4You.class.getResource("event-preview-view.fxml"));
            try{
                child = fxmlLoader.load();
            }catch (Exception ignore){}
            EventPreviewController lvc = fxmlLoader.getController();
            lvc.setModel(this.model, e.id());
            lvc.build(e);
            hBoxParaAsPresencas.getChildren().add(child);
        }

        apParaAsPresencas.setPrefWidth(350 * (usersThatWentToTheEvent.size() + 1));
        hBoxParaAsPresencas.setPrefWidth(350 * (usersThatWentToTheEvent.size() + 1));
    }

    public void update(){
        stackPaneMenus.setVisible(model != null && model.getState() == State.SELECT_USERS);
        if (stackPaneMenus.isVisible())
            _setUpUsers();

    }

    private void _setUpUsers() {
        this.usersList.getItems().clear();
        this.usersList.getItems().setAll(model.getUsersForComboBox());
    }
}
