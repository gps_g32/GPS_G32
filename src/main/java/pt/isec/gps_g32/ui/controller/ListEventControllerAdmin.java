package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.user.Filter;
import pt.isec.gps_g32.ui.IPC4You;
import pt.isec.gps_g32.model.data.state.State;
import java.io.IOException;
import java.util.Set;

public class ListEventControllerAdmin extends Controller {
    @FXML
    public ImageView imgCreate;

    @FXML
    RadioButton all;

    @FXML
    RadioButton interessado;

    @FXML
    RadioButton naoInteressado;

    @FXML
    StackPane stackPaneMenus;

    @FXML
    AnchorPane eventScrollAnchorPane;

    @FXML
    VBox vBoxParaEventos;

    @FXML
    TextField eventName;

    @FXML
    private void initialize () throws IOException {
        System.out.println("ListEventController initialized");
    }

    public void setModel(ModelManager model) {
        this.model = model;
        _update();
    }

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        model.addPropertyChangeListener(ModelManager.PROP_DATA, evt -> {
            _update();
        });
        update();
    }

    private void _update(){
        vBoxParaEventos.getChildren().clear();
        Set<Event> events = this.model.getEvents();
        if (events == null)
            return;
        try{
            for (Event e: events) {
                if (!e.isPast()) {
                    if (!e.getName().contains(eventName.getText())) {
                        continue;
                    }
                    FXMLLoader fxmlLoader;
                    fxmlLoader = new FXMLLoader(IPC4You.class.getResource("event-preview-view.fxml"));
                    Parent child = fxmlLoader.load();
                    EventPreviewController lvc = fxmlLoader.getController();
                    lvc.setModel(this.model, e.getId());
                    lvc.registerHandlers();
                    lvc.build(e);
                    vBoxParaEventos.getChildren().add(child);
                }
            }
        }catch (Exception e){
            System.out.println(e);
        }

//        eventScrollAnchorPane.setPrefHeight(165 * events.size());
//        vBoxParaEventos.setPrefHeight(165 * events.size());
    }

    public void update(){
        boolean isMenuAdmin = model !=null &&model.getState() == State.MENU_ADMIN && model.getLoggedIsAdmin();
        stackPaneMenus.setVisible(isMenuAdmin);
        if (isMenuAdmin)
            _update();
    }

    public void handleClick(MouseEvent mouseEvent) {
        if (mouseEvent.getSource() == all){
            _processAllClicked();
        }
        else if(mouseEvent.getSource() == naoInteressado) {
            System.out.println("Eventos em que nao estou interessado");
            _processNotInterestedClicked();
        }
        else if(mouseEvent.getSource() == interessado){
            System.out.println("Eventos do meu interesse");
            _processInterested();
        }
    }

    private void _processInterested() {
        this.all.setSelected(false);
        this.naoInteressado.setSelected(false);
        model.setCurrentEventFilter(Filter.INTERESTED);
    }

    private void _processNotInterestedClicked() {
        this.all.setSelected(false);
        this.interessado.setSelected(false);
        model.setCurrentEventFilter(Filter.NOT_INTERESTED);
    }

    private void _processAllClicked() {
        this.naoInteressado.setSelected(false);
        this.interessado.setSelected(false);
        model.setCurrentEventFilter(Filter.ALL_EVENTS);
        System.out.println(model.getLoggedUser().getUserType());
    }

    public void handleGoToEventsOrUsers(MouseEvent mouseEvent) {

    }

    public void handleLookForEvents(MouseEvent mouseEvent) {
        System.out.println("A procurar os eventos:" + this.eventName.getText());
        _update();
    }


}