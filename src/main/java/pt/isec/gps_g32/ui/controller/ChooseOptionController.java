package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.event.EventData;
import pt.isec.gps_g32.model.data.state.State;
import pt.isec.gps_g32.model.data.user.Interest;

import java.util.ArrayList;

public class ChooseOptionController extends Controller{
    //Temporary
    
    @FXML
    public StackPane stackPaneMenus;

    public void setModel(ModelManager model) {
        this.model = model;
    }

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        update();
    }
    public void update(){
        stackPaneMenus.setVisible(model != null && model.getState() == State.CHOOSE);
        System.out.println("Criar evento");
    }

    public void handleGoToChoose(MouseEvent mouseEvent) {
        this.model.changeState(State.CHOOSE);
    }
    public void handleGoToEvents(MouseEvent mouseEvent) {
        this.model.changeState(State.SELECT_EVENTS);
    }
    public void handleGoToUsers(MouseEvent mouseEvent) {
        this.model.changeState(State.SELECT_USERS);
    }
}
