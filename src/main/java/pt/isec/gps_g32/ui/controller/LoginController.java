package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.state.State;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The type Login view controller.
 */
public class LoginController extends Controller {
    private static final String CLOSE_LABEL = "Fechar";
    private static final String RECOVER_LABEL = "Recuperar";
    private static final String CODE_LABEL = "Tenho o código";
    @FXML private StackPane stackPaneLogin;
    @FXML private TextField emailField;
    @FXML private PasswordField passwordField;
    private Alert invalidePopUp, recoverPopUp, codePopUp, invalideCodePopUp;
    private ButtonType btnRecover, btnClose, btnCode;
    private TextField emailFieldPopUp, codeFieldPopUp;
    private AtomicBoolean continueRecover, continueCode;

    @FXML
    private void initialize() {
        configurePopUps();
        configureFields();
        continueRecover = new AtomicBoolean(false);
        continueCode = new AtomicBoolean(false);
        emailField.setOnKeyPressed(this::handleEnterKey);
        passwordField.setOnKeyPressed(this::handleEnterKey);
    }

    @FXML
    private void handleEnterKey(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) onLogin();
    }

    private void configurePopUps() {
        btnClose = new ButtonType(CLOSE_LABEL);
        btnRecover = new ButtonType(RECOVER_LABEL);
        btnCode = new ButtonType(CODE_LABEL);

        invalidePopUp = createAlert("Credências inválidas", "As credências adicionas são inválidas", "Por favor tente novamente", btnClose);
        invalidePopUp.setAlertType(Alert.AlertType.ERROR);

        recoverPopUp = createAlert("Recuperar password", "Recuperar password", "Insira o seu email para recuperar a sua password", btnCode, btnRecover, btnClose);

        codePopUp = createAlert("Recuperar password", "Foi enviado o código para recuperar o código para o seu email. Insira o código no campo abaixo", null, btnRecover, btnClose);

        invalideCodePopUp = createAlert("Códgio inválidas", "O códgio introduzido é inválidas", "Por favor tente novamente", btnClose);
        invalideCodePopUp.setAlertType(Alert.AlertType.ERROR);
    }

    private Alert createAlert(String title, String header, String content, ButtonType... buttonTypes) {
        Alert alert = new Alert(Alert.AlertType.NONE);
        alert.setTitle(title);
        alert.setHeaderText(header);
        if (content != null) {
            alert.setContentText(content);
        }
        alert.getButtonTypes().setAll(buttonTypes);
        return alert;
    }

    private void configureFields() {
        emailFieldPopUp = new TextField();
        emailFieldPopUp.setPromptText("Email");
        recoverPopUp.getDialogPane().setContent(emailFieldPopUp);

        codeFieldPopUp = new TextField();
        codeFieldPopUp.setPromptText("Código");
        codePopUp.getDialogPane().setContent(codeFieldPopUp);

    }

    /**
     * Register handlers.
     */
    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> update());
        update();
    }

    public void update() {
        stackPaneLogin.setVisible(model != null && model.getState() == State.LOGIN);
    }

    /**
     * On login.
     */
    @FXML
    public void onLogin() {
        String email = emailField.getText();
        String password = passwordField.getText();

        if (checkField(email, emailField) || checkField(password, passwordField)) return;

        boolean authenticated = model.authenticate(email, password);

        if (!authenticated) {
            showInvalidCredentialsPopUp();
        } else
            model.setLoggerUser(email);

        if(model.getLoggedIsAdmin()){
            model.changeState(State.MENU_ADMIN);
        }
        else if(authenticated){
            model.changeState(State.MENU);
        }
    }

    /**
     * On forgot pass.
     */
    @FXML
    public void onForgotPass() {
        continueRecover.set(true);
        while (continueRecover.get()) {
            handleRecoverButton();
        }
    }

    private void handleRecoverButton() {
        recoverPopUp.showAndWait().ifPresent(buttonType -> {
            if (buttonType == btnRecover) {
                String email = emailFieldPopUp.getText();
                if (!model.validateEmail(email)) {
                    showInvalidCredentialsPopUp();
                } else {
                    model.createUserCode(email);
                    handleCodeDialog(email);
                }
            } else if(buttonType == btnCode) {
                String email = emailFieldPopUp.getText();
                if (!model.validateEmail(email)) {
                    showInvalidCredentialsPopUp();
                } else {
                    handleCodeDialog(email);
                }
            } else if (buttonType == btnClose) {
                continueRecover.set(false);
            }
        });
    }

    private void handleCodeDialog(String email) {
        continueCode.set(true);
        while (continueCode.get()) {
            codePopUp.showAndWait().ifPresent(buttonType -> {
                if (buttonType == btnRecover) {
                    String code = codeFieldPopUp.getText();
                    if (!model.validateCode(email, code)) {
                        showInvalidCodePopUp();
                    } else {
                        continueRecover.set(false);
                        continueCode.set(false);
                        model.setLoggerUser(email);
                        model.changeState(State.MENU);
                    }
                } else if (buttonType == btnClose) {
                    continueRecover.set(false);
                    continueCode.set(false);
                }
            });
        }
    }

    private void showInvalidCredentialsPopUp() {
        invalidePopUp.showAndWait();
        clearFields();
    }

    private void showInvalidCodePopUp() {
        invalideCodePopUp.showAndWait();
    }

    private void clearFields() {
        emailField.clear();
        passwordField.clear();
    }

    private boolean checkField(String string, TextField field) {
        if (string.isEmpty()) {
            field.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
            update();
            return true;
        } else {
            field.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        }
        return false;
    }
}
