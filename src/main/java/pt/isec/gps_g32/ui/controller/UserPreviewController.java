package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.text.Text;
import pt.isec.gps_g32.model.data.user.User;

import java.io.IOException;

public class UserPreviewController extends Controller{
    @FXML Text nomeUtilizador;
    @FXML  Text emailUtilizador;


    @FXML
    private void initialize () {

    }

    public void setup(User user){
        nomeUtilizador.setText(user.getName());
        emailUtilizador.setText(user.getEmail());
    }
}
