package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.ui.IPC4You;
import pt.isec.gps_g32.model.data.state.State;

import java.util.List;

public class EventHistoryViewController extends Controller {

    @FXML
    public StackPane stackPaneMenus;
    @FXML
    public VBox vBoxParaEventos;
    @FXML
    public AnchorPane eventScrollAnchorPane;
    @FXML
    public ScrollPane scrollPane;
    ModelManager model;

    public void setModel(ModelManager model) {
        this.model = model;
        registerHandlers();

        //TODO: verifica isto (update e _update)
        update();
        _update();
    }

    private void _update(){

        List<Event> events = this.model.getEventHistory();
        if (events == null || events.isEmpty())
            return;

        vBoxParaEventos.getChildren().clear();
        try{
            for (Event e: events) {
                if (e.isPast()) {
                    FXMLLoader fxmlLoader;
                    fxmlLoader = new FXMLLoader(IPC4You.class.getResource("old-event-preview-view.fxml"));
                    Parent child = fxmlLoader.load();
                    PastEventPreviewController lvc = fxmlLoader.getController();
                    lvc.setModel(this.model, e.getId());
                    lvc.registerHandlers();
                    lvc.build(e);
                    vBoxParaEventos.getChildren().add(child);
                }
            }

            eventScrollAnchorPane.setPrefHeight(165 * events.size());
            vBoxParaEventos.setPrefHeight(165 * events.size());


        }catch (Exception e){
            System.out.println(e);
        }

    }

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
            //_update();
        });
        model.addPropertyChangeListener(ModelManager.PROP_DATA, evt -> {
            _update();
        });
    }
    public void update(){
        stackPaneMenus.setVisible(model != null && model.getState() == State.HISTORY);
        if (stackPaneMenus.isVisible())
            _update();
        System.out.println("Historico de eventos");
    }


    public void handleGoToMenu(MouseEvent mouseEvent) {
        System.out.println("CHANGE STATE TO MENU");
        if (model.getLoggedIsAdmin())
            model.changeState(State.MENU_ADMIN);
        else
            model.changeState(State.MENU);
    }

    public void handleGoToCreate(MouseEvent mouseEvent) {
        if (this.model.getLoggedIsAdmin())
            model.changeState(State.CREATE_EVENT);
        else
            model.changeState(State.CREATE_EVENT_REQUEST);
    }
//
    public void handleGoToAccount(MouseEvent mouseEvent) {
        model.changeState(State.ACCOUNT_INFO);
    }

//    public void handleGoToEventRequests(MouseEvent mouseEvent) {
//        if (model.getLoggedIsAdmin())
//            model.changeState(State.LIST_EVENT_REQUESTS);
//    }
}
