package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.state.State;
import pt.isec.gps_g32.model.data.user.User;
import pt.isec.gps_g32.ui.IPC4You;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EventDetailsController extends Controller {
    boolean changes;

    @FXML
    Text home2;

    @FXML
    VBox create;

    @FXML
    VBox attendance;

    @FXML
    VBox history;

    @FXML
    VBox home;

    @FXML
    VBox account;

    @FXML
    StackPane stackPane;

    @FXML
    ImageView eventImage;

    @FXML
    TextArea eventName;

    @FXML
    ComboBox eventType;

    @FXML
    DatePicker eventDate;

    @FXML
    ComboBox eventHour;

    @FXML
    ComboBox eventRoom;

    @FXML
    TextField eventLink;

    @FXML
    TextArea eventNotes;

    @FXML
    ComboBox eventOwner;

    @FXML
    CheckBox informatica;

    @FXML
    CheckBox egi;

    @FXML
    CheckBox bioengenharia;

    @FXML
    CheckBox mecanica;

    @FXML
    CheckBox civil;

    @FXML
    CheckBox eletromecanica;

    @FXML
    CheckBox engenharia;

    @FXML
    CheckBox softskills;

    @FXML
    CheckBox licenciaturas;

    @FXML
    CheckBox mestrados;

    @FXML
    CheckBox ctesp;

    @FXML
    CheckBox associativismo;

    @FXML
    private Map<String, CheckBox> checkBoxMap = new HashMap<>();

    @FXML private VBox ratingSection;
    @FXML private Slider ratingSlider;
    @FXML private Text ratingText;

    @FXML private ComboBox eventDuration;

    private Event event;

    @FXML
    public void initialize() {
        checkBoxMap.put("informatica", informatica);
        checkBoxMap.put("egi", egi);
        checkBoxMap.put("bioengenharia", bioengenharia);
        checkBoxMap.put("mecanica", mecanica);
        checkBoxMap.put("civil", civil);
        checkBoxMap.put("eletromecanica", eletromecanica);
        checkBoxMap.put("engenharia", engenharia);
        checkBoxMap.put("softskills", softskills);
        checkBoxMap.put("licenciaturas", licenciaturas);
        checkBoxMap.put("mestrados", mestrados);
        checkBoxMap.put("ctesp", ctesp);
        checkBoxMap.put("associativismo", associativismo);

        ratingText.textProperty().bindBidirectional(ratingSlider.valueProperty(), new java.text.DecimalFormat("#.#"));

        //TODO: só avaliar eventos passados
        /*if (oldEvent) {
            ratingSection.setVisible(true);
        }
        else {
            ratingSection.setVisible(false);
        }*/
    }

    public void setModel(ModelManager model) {
        this.model = model;
        for (User user : model.getUsers()) eventOwner.getItems().add(user.getName());

        registerHandlers();
        update();
    }

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
    }

    public void update(){
        boolean visible = (model != null && model.getState() == State.CHECK_EVENT);
        stackPane.setVisible(visible);
        if(visible) _update();
    }

    private void _update() {
        try {
            event = model.getActiveEvent();
            this.changes = false;
            Image eventImg = new Image(Objects.requireNonNull(IPC4You.class.getResource("/pt/isec/gps_g32/images/" + event.getImagePath())).toExternalForm());
            eventImage.setImage(eventImg);

            eventName.setText(event.getName());
            eventType.getSelectionModel().select(event.getType());

            String dateStr = event.getDate();

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date = LocalDate.parse(dateStr, dateFormatter);
            eventDate.setValue(date);

            eventHour.getSelectionModel().select(event.getHour());
            eventRoom.getSelectionModel().select(event.getLocal());

            eventLink.setText(event.getLink() != null ? event.getLink() : "");

            for (CheckBox checkBox : checkBoxMap.values()) {
                checkBox.setSelected(false);
            }

            String interestsString = event.getInterests();
            String[] interestsArray = interestsString.split(";");

            for (String interest : interestsArray) {
                CheckBox checkBox = checkBoxMap.get(interest.toLowerCase());
                checkBox.setSelected(true);
            }
            eventNotes.setText(event.getDescription());

            for (User user : model.getUsers()) {
                if (user.getEmail().equals(event.getResponsible())) {
                    eventOwner.getSelectionModel().select(user.getName());
                    break;
                }
            }

            eventDuration.getSelectionModel().select(event.getDurationInMinutes());

            if (event.getUserRating(model.getLoggedUser().getEmail()) != null)
                ratingSlider.setValue(event.getUserRating(model.getLoggedUser().getEmail()));
            else
                ratingSlider.setValue(0);

            // Disable all fields
            eventName.setDisable(true);
            eventType.setDisable(true);
            eventDate.setDisable(true);
            eventHour.setDisable(true);
            eventRoom.setDisable(true);
            eventLink.setDisable(true);
            eventNotes.setDisable(true);
            eventOwner.setDisable(true);
            eventDuration.setDisable(true);

            //TODO: remover esta merda
            ratingSlider.setVisible(true);
            ratingText.setVisible(true);
            ratingSection.setVisible(true);

            for (CheckBox checkBox : checkBoxMap.values()) {
                checkBox.setDisable(true);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Alert confirmation;
            stackPane.setVisible(false);
            confirmation = new Alert(Alert.AlertType.ERROR);
            confirmation.setTitle("Erro");
            confirmation.setHeaderText("Evento não encontrado!");
            confirmation.setContentText("O evento não foi encontrado.");
            confirmation.showAndWait();
            model.changeState(State.MENU);
        }
    }

    public void goHome(String vboxId) {
        switchView(vboxId);
    }

    public void getButtonPressed(MouseEvent event) {
        Object source = event.getSource();
        String vboxId = null;

        if (source instanceof VBox) {
            VBox clickedVBox = (VBox) source;
            vboxId = clickedVBox.getId();
        }
        else if(source instanceof Text) {
            Text clickedText = (Text) source;
            vboxId = clickedText.getId();
        }

        saveRating();
        goHome(vboxId);
    }

    private void switchView(String vboxId) {
        switch(vboxId) {
            case "home":
            case "home2":
                model.changeState(State.MENU);
                break;
            case "history":
                model.changeState(State.HISTORY);
                break;
            case "attendance":
                model.changeState(State.ATTENDANCE);
                break;
            case "create":
                if(model.getLoggedIsAdmin()) model.changeState(State.CREATE_EVENT);
                else model.changeState(State.CREATE_EVENT_REQUEST);
                break;
            case "account":
                model.changeState(State.ACCOUNT_INFO);
                break;
        }
    }

    private void saveRating() {
        if (ratingSection.isVisible()) {
            //event.addRating(model.getLoggedUser().getEmail(), ratingSlider.getValue());
            model.addUserRating(ratingSlider.getValue());
        }
    }
}
