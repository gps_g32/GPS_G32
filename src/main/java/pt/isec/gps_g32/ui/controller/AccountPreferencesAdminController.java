package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.state.State;

public class AccountPreferencesAdminController extends Controller{


    @FXML
    StackPane stackPane;

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        update();
    }

    @Override
    protected void update() {
        this.stackPane.setVisible(model != null && model.getState() == State.ACCOUNT_INFO_ADMIN);
    }
}
