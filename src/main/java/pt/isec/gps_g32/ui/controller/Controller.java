package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.state.State;

public abstract class Controller {
    protected ModelManager model;

    /**
     * Sets model.
     *
     * @param model the model
     */
    public void setModel(ModelManager model) {this.model = model;}

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
    }
    protected void update(){

    }

    public void handleGoToMenu(MouseEvent mouseEvent){
        if (this.model.getLoggedIsAdmin())
            model.changeState(State.MENU_ADMIN);
        else
            model.changeState(State.MENU);
    }

    public void handleGoToChoose(MouseEvent mouseEvent) {
        this.model.changeState(State.CHOOSE);
    }

    public void handleGoToHistory(MouseEvent mouseEvent){
        if(this.model.getLoggedIsAdmin())
            model.changeState(State.CHOOSE);
        else
            model.changeState(State.HISTORY);
    }

    public void handleGoToCreate(MouseEvent mouseEvent) {
        if (this.model.getLoggedIsAdmin())
            model.changeState(State.CREATE_EVENT);
        else
            model.changeState(State.CREATE_EVENT_REQUEST);
    }

    public void handleGoToEventRequests(MouseEvent mouseEvent) {
        model.changeState(State.LIST_EVENT_REQUESTS);
    }

    public void handleGoToAccountInfo(MouseEvent mouseEvent) {

        if (this.model.getLoggedIsAdmin())
            model.changeState(State.ACCOUNT_INFO_ADMIN);
        else
            model.changeState(State.ACCOUNT_INFO);    }
}