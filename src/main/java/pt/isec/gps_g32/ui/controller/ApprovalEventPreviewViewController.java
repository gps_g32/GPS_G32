package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.Interest;
import pt.isec.gps_g32.ui.IPC4You;

import java.util.ArrayList;
import java.util.Objects;

public class ApprovalEventPreviewViewController extends Controller {

    @FXML
    public ImageView ImagemDoEvento;
    @FXML
    public Text NomeEvent;
    @FXML
    public Text interesses;
    @FXML
    public Text tipo;
    @FXML
    public Text local;
    @FXML
    public Button btnAcceptRequest;
    @FXML
    public Button btnDenyRequest;
    @FXML
    public CheckBox cbAdmin;

    ModelManager model;
    int eventID;

    public void setModel(ModelManager model, int eventID) {
        this.model = model;
        this.eventID = eventID;
    }

    public void registerHandlers() {

    }

    public void build(Event event) {
        StringBuilder interessesStr = new StringBuilder();
        this.NomeEvent.setText(event.getName());
        this.tipo.setText(event.getType());
        this.local.setText(event.getLocal());
        ArrayList<Interest> interests = event.getInterests(true);
        for (Interest interest : interests) {
            interessesStr.append(interest.toString()).append(" | ");
        }
        this.interesses.setText(interests.toString());
        System.out.println(event.getImagePath());
        Image eventoImg = new Image(Objects.requireNonNull(IPC4You.class.getResource("/pt/isec/gps_g32/images/" + event.getImagePath())).toExternalForm());
        ImagemDoEvento.setImage(eventoImg);

    }

    @FXML
    public void handleAcceptRequest(MouseEvent actionEvent) {
        model.processEventRequest(eventID, true, cbAdmin.isSelected());
    }

    @FXML
    public void handleDenyRequest(MouseEvent actionEvent) {
        model.processEventRequest(eventID, false, false);
    }

    @FXML
    public void handleShowEventDetails(MouseEvent mouseEvent) {
    }

}
