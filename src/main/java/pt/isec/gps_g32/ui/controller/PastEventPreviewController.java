package pt.isec.gps_g32.ui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.state.State;

import java.io.IOException;

public class PastEventPreviewController extends Controller{

    @FXML
    public Text lbNomeEvent;
    @FXML
    public Text lbTipo;
    @FXML
    public Text lbLocal;
    @FXML
    public Text lbDateTime;
    @FXML
    ImageView ImagemDoEvento;
    @FXML
    public Button btnGetPresence;

    ModelManager model;
    int eventID;

    @FXML
    private void initialize () throws IOException {
        System.out.println("PastEventPreviewController initialized");
    }

    public void setModel(ModelManager model, int eventID) {
        this.model = model;
        this.eventID = eventID;
    }

    public void registerHandlers() {}

    public void update() {
        //TODO: DEBUG
        System.out.println("Create past event preview");
    }

    public void build(Event event){
        //System.out.println(event.getImagePath());
        if (event.getLink() != null)
            this.lbLocal.setText(event.getLocal() + " | Online");
        else
            this.lbLocal.setText(event.getLocal());
        this.lbNomeEvent.setText(event.getName());
        this.lbDateTime.setText(event.getHour() + " " + event.getDate());
        this.lbTipo.setText(event.getType());
        /*Image eventoImg = new Image(Objects.requireNonNull(IPC4You.class.getResource("images/" + event.getImagePath())).toExternalForm());
        if ( event.getImagePath() != null)
            ImagemDoEvento.setImage(eventoImg);*/
    }

    public void handleGetPresence(ActionEvent actionEvent) {
        System.out.println("Dounload presence certificate");
    }

    public void openEventDetails() {
        model.clickedEvent(this.eventID);
        model.changeState(State.CHECK_EVENT);
    }
}
