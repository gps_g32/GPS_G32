package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.Interest;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.ui.IPC4You;
import pt.isec.gps_g32.model.data.state.State;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class EventPreviewController extends Controller {
    public Text dateTimetext;
    @FXML
    private Text interesses;
    @FXML
    private Text tipo;
    @FXML
    private Text NomeEvent;
    @FXML
    private ImageView star;
    @FXML
    private ImageView participate;
    @FXML
    ImageView ImagemDoEvento;
    @FXML
    Text local;

    @FXML
    HBox hBoxlive;

    ModelManager model;
    int eventID;

    @FXML
    private void initialize () throws IOException {
        System.out.println("EventPreviewController initialized");
    }

    public void setModel(ModelManager model, int eventID) {
        this.model = model;
        this.eventID = eventID;
    }

    public void registerHandlers() {

    }

    public void build(Event event) {
        StringBuilder interessesStr = new StringBuilder();
        this.NomeEvent.setText(event.getName());
        this.tipo.setText(event.getType());
        this.local.setText(event.getLocal());
        this.dateTimetext.setText(event.getHour() + " " + event.getDate());
        
        ArrayList<Interest> interests = event.getInterests(true);
        for (Interest interest : interests) {
            interessesStr.append(interest.toString()).append(" | ");
        }
        this.interesses.setText(interests.toString());
        System.out.println(event.getImagePath());
        Image eventoImg = new Image(Objects.requireNonNull(IPC4You.class.getResource("/pt/isec/gps_g32/images/" + event.getImagePath())).toExternalForm());
        ImagemDoEvento.setImage(eventoImg);

        if(model.getFavourite(this.eventID)) {
            Image starImg = new Image(Objects.requireNonNull(IPC4You.class.getResource("/pt/isec/gps_g32/icons/star_filled.png")).toExternalForm());
            star.setImage(starImg);
        }

        if (model.isParticipating(this.eventID)) {
            Image participateImg = new Image(Objects.requireNonNull(IPC4You.class.getResource("/pt/isec/gps_g32/icons/participate_filled.png")).toExternalForm());
            participate.setImage(participateImg);
        }

        hBoxlive.setVisible(event.isLive());
    }
    
    public void csEditEvent(MouseEvent mouseEvent) {
        model.clickedEvent(this.eventID);
        if(model.getLoggedIsAdmin()) model.changeState(State.EDIT_EVENT);
        else model.changeState(State.CHECK_EVENT);
    }

    public void checkFavouriteStatus(MouseEvent mouseEvent) {
        if (model.isFavourite(this.eventID)) {
            model.removeFavourite(this.eventID);
        } else {
            model.addFavourite(this.eventID);
        }
        model.saveUsers();
    }

    public void checkParticipation(MouseEvent mouseEvent) {
        if (model.isParticipating(this.eventID)) {
            model.removeParticipation(this.eventID);
        } else {
            model.addParticipation(this.eventID);
        }
        model.saveEvents();
    }
}
