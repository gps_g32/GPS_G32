package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.state.State;
import pt.isec.gps_g32.model.data.user.User;
import pt.isec.gps_g32.ui.IPC4You;
import java.util.List;

public class SelectEventsController  extends Controller{
    //Temporary
    @FXML
    public StackPane stackPaneMenus;
    @FXML
    public ComboBox existingEvents;
    @FXML public AnchorPane apUsersFavoritaramEvento;
    @FXML public AnchorPane apUsersForamAoEvento;

    @FXML
    HBox hBoxParaOsUtilizadoresQueFavoritaramOEvento;

    @FXML
    HBox hBoxParaUtilizadoresQueForamAoEvento;

    public void setModel(ModelManager model) {
        this.model = model;

        hBoxParaOsUtilizadoresQueFavoritaramOEvento.getChildren().clear();
        hBoxParaUtilizadoresQueForamAoEvento.getChildren().clear();
    }
    
    public void registerHandlers() {
        this.existingEvents.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue!= null) {
                        System.out.println("A ir buscar os utilizadores do evento" + newValue);
                        _changeContent(newValue.toString());
                    }
                }
        );

        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        update();
    }

    private void _changeContent(String newValue) {
        List<User> usersThatHaveEventFavorite = model.getUsersThatHaveEventFavorite(Integer.parseInt(newValue));
        List<User> usersThatWentToTheEvent = model.getUsersThatWentToEvent(Integer.parseInt(newValue));

        hBoxParaOsUtilizadoresQueFavoritaramOEvento.getChildren().clear();
        hBoxParaUtilizadoresQueForamAoEvento.getChildren().clear();

        if (!usersThatWentToTheEvent.isEmpty())
            _setPresences(usersThatWentToTheEvent);
        if(!usersThatHaveEventFavorite.isEmpty())
            _setFavorites(usersThatHaveEventFavorite);
    }

    private void _setFavorites(List<User> usersThatHaveEventFavorite) {
        for (User e: usersThatHaveEventFavorite) {
            Parent child = null;
            FXMLLoader fxmlLoader;
            fxmlLoader = new FXMLLoader(IPC4You.class.getResource("user-preview-view.fxml"));
            try{
                child = fxmlLoader.load();
            }catch (Exception ignore){}
            UserPreviewController lvc = fxmlLoader.getController();
            lvc.setup(e);
            hBoxParaOsUtilizadoresQueFavoritaramOEvento.getChildren().add(child);
        }

        apUsersFavoritaramEvento.setPrefWidth(350 * (usersThatHaveEventFavorite.size() + 1));
        hBoxParaOsUtilizadoresQueFavoritaramOEvento.setPrefWidth(350 * (usersThatHaveEventFavorite.size() + 1));
    }

    private void _setPresences(List<User> usersThatWentToTheEvent) {
        for (User e: usersThatWentToTheEvent) {
            Parent child = null;
            FXMLLoader fxmlLoader;
            fxmlLoader = new FXMLLoader(IPC4You.class.getResource("user-preview-view.fxml"));
            try{
                child = fxmlLoader.load();
            }catch (Exception ignore){}
            UserPreviewController lvc = fxmlLoader.getController();
            lvc.setup(e);
            hBoxParaUtilizadoresQueForamAoEvento.getChildren().add(child);
        }

        apUsersForamAoEvento.setPrefWidth(350 * (usersThatWentToTheEvent.size() + 1));
        hBoxParaUtilizadoresQueForamAoEvento.setPrefWidth(350 * (usersThatWentToTheEvent.size() + 1));
    }

    public void update(){
        stackPaneMenus.setVisible(model != null && model.getState() == State.SELECT_EVENTS);
        if (stackPaneMenus.isVisible())
            _setSelectableEvents();
    }

    private void _setSelectableEvents(){
        existingEvents.getItems().clear();
        existingEvents.getItems().setAll(model.getEventsForComboBox());
    }

    private void _handleGoChangeEvent(){

    }
}
