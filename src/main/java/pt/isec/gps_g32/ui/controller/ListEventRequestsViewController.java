package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.state.State;
import pt.isec.gps_g32.model.data.user.Filter;
import pt.isec.gps_g32.ui.IPC4You;

import java.io.IOException;
import java.util.Set;

public class ListEventRequestsViewController extends Controller{

    @FXML
    public ImageView imgCreate;

    @FXML
    RadioButton all;

    @FXML
    RadioButton interessado;

    @FXML
    RadioButton naoInteressado;

    @FXML
    StackPane stackPaneMenus;

    @FXML
    AnchorPane eventScrollAnchorPane;

    @FXML
    VBox vBoxParaEventos;

    @FXML
    private void initialize () throws IOException {
        System.out.println("ListEventController initialized");
    }

    public void setModel(ModelManager model) {
        this.model = model;
        _update();
    }

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        model.addPropertyChangeListener(ModelManager.PROP_DATA, evt -> {
            _update();
        });
        update();
    }

    private void _update(){
        vBoxParaEventos.getChildren().clear();
        Set<Event> events = this.model.getEventRequests();
        if (events == null)
            return;

        try{
            for (Event e: events) {
                FXMLLoader fxmlLoader;
                fxmlLoader = new FXMLLoader(IPC4You.class.getResource("approval-event-preview-view.fxml"));
                Parent child = fxmlLoader.load();
                ApprovalEventPreviewViewController lvc = fxmlLoader.getController();
                lvc.setModel(this.model, e.getId());
                lvc.registerHandlers();
                lvc.build(e);
                vBoxParaEventos.getChildren().add(child);
            }
        }catch (Exception e){
            System.out.println("AQUI -" + e);
        }

        eventScrollAnchorPane.setPrefHeight(165 * events.size());
        vBoxParaEventos.setPrefHeight(165 * events.size());

    }

    public void update(){
        boolean isMenuAdmin = model !=null &&model.getState() == State.LIST_EVENT_REQUESTS;
        stackPaneMenus.setVisible(isMenuAdmin);
        if (isMenuAdmin)
            _update();
    }

    public void csEditEvent(MouseEvent mouseEvent) {
    }

}
