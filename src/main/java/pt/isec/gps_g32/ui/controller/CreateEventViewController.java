package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.Interest;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.EventData;
import pt.isec.gps_g32.model.data.state.State;

import java.util.ArrayList;

public class CreateEventViewController extends Controller {

    //Temporary
    EventData eventData;

    @FXML
    public Pane btnBack;
    @FXML
    public TextArea taName;
    @FXML
    public ComboBox<String> comboType;  //Event type
    @FXML
    public DatePicker dpDate;
    @FXML
    public ComboBox<String> comboTime;
    @FXML
    public ComboBox<String> comboSala;
    @FXML
    public CheckBox cbLink;
    @FXML
    public TextField tfLink;
    //***********Interesses*****************
    @FXML
    public CheckBox cbInformatica;
    @FXML
    public CheckBox cbEGI;
    @FXML
    public CheckBox cbBioengenharia;
    @FXML
    public CheckBox cbMecanica;
    @FXML
    public CheckBox cbCivil;
    @FXML
    public CheckBox cbEletromecanica;
    @FXML
    public CheckBox cbEngenharia;
    @FXML
    public CheckBox cbSoftSkills;
    @FXML
    public CheckBox cbLicenciaturas;
    @FXML
    public CheckBox cbMestrados;
    @FXML
    public CheckBox cbCTESP;
    @FXML
    public CheckBox cbAssociativismo;
    //*************************************
    @FXML
    public TextArea taNotas;
    @FXML
    public TextField tfResponsible;
    @FXML
    public CheckBox cbAdmin;
    @FXML
    public CheckBox cbVisibility;
    @FXML
    public ImageView ivSave;
    @FXML
    public StackPane stackPaneMenus;

    @FXML
    public ComboBox<String> comboDuration;


    public void setModel(ModelManager model) {
        eventData = new EventData();
        this.model = model;

        setUp();
        registerHandlers();
        update();
    }

    private void setUp() {
        for (String s : eventData.getEventTypes())
            comboType.getItems().add(s);

        for (String s : eventData.getEventLocations())
            comboSala.getItems().add(s);

        for (String s : eventData.getPossibleTimes())
            comboTime.getItems().add(s);

        for (String s : eventData.getPossibleDuration())
            comboDuration.getItems().add(s);

    }

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        //update();
    }
    public void update(){
        stackPaneMenus.setVisible(model != null && model.getState() == State.CREATE_EVENT);
        System.out.println("Criar evento");
    }

    public void handleBack(MouseEvent mouseEvent) {
        System.out.println("Back");
    }

    public void handleSave(MouseEvent mouseEvent) {
        System.out.println("Create Event Pressed");
        if (!checkFields()) {
            fieldsErrorPopup(null);
            return;
        }
        ArrayList<Interest> interests = createInterests();

        Event eventTemp = new Event(taName.getText(), comboType.getValue(), dpDate.getValue().toString(), comboTime.getValue(),
                interests, tfResponsible.getText(), cbAdmin.isSelected(), taNotas.getText(), !cbVisibility.isSelected(),comboSala.getValue(),
                tfLink.getText(), null, "evento3.png", Integer.parseInt(comboDuration.getValue().toString()), 0.0, new ArrayList<>());

        System.out.println(eventTemp);
        //model.createEvent(eventTemp);
        confirmationPopup(null, eventTemp);
    }

    private void fieldsErrorPopup(MouseEvent mouseEvent) {
        Alert confirmation;
        confirmation = new Alert(Alert.AlertType.ERROR);
        confirmation.setTitle("Erro");
        confirmation.setHeaderText("Alterações não guardadas!");
        confirmation.setContentText("Existe um ou mais campo(s) sem preenchimento ou com informação inválida.");
        confirmation.showAndWait();
    }

    private void confirmationPopup(MouseEvent mouseEvent, Event event) {

        Alert confirmation;
        if(model.createEvent(event)) {
            confirmation = new Alert(Alert.AlertType.INFORMATION);
            confirmation.setTitle("Sucesso");
            confirmation.setHeaderText("Alterações guardadas com sucesso!");
            confirmation.setContentText("As alterações foram guardadas e o evento foi atualizado com sucesso.");
            confirmation.showAndWait();
            model.changeState(State.MENU_ADMIN);
        }
        else {
            confirmation = new Alert(Alert.AlertType.ERROR);
            confirmation.setTitle("Erro");
            confirmation.setHeaderText("Ocorreu um erro!");
            confirmation.setContentText("NOT FOUND.");
            confirmation.showAndWait();
        }


    }

    private ArrayList<Interest> createInterests() {
        ArrayList<Interest> interests = new ArrayList<>();

        if (cbInformatica.isSelected())
            interests.add(Interest.INFORMATICA);

        if (cbEGI.isSelected())
            interests.add(Interest.EGI);

        if (cbBioengenharia.isSelected())
            interests.add(Interest.BIOENGENHARIA);

        if (cbMecanica.isSelected())
            interests.add(Interest.MECANICA);

        if (cbCivil.isSelected())
            interests.add(Interest.CIVIL);

        if (cbEletromecanica.isSelected())
            interests.add(Interest.ELETROMECANICA);

        if (cbEngenharia.isSelected())
            interests.add(Interest.ENGENHARIA);

        if (cbSoftSkills.isSelected())
            interests.add(Interest.SOFTSKILLS);

        if (cbLicenciaturas.isSelected())
            interests.add(Interest.LICENCIATURAS);

        if (cbMestrados.isSelected())
            interests.add(Interest.MESTRADOS);

        if (cbCTESP.isSelected())
            interests.add(Interest.CTESP);

        if (cbAssociativismo.isSelected())
            interests.add(Interest.ASSOCIATIVISMO);

        return interests;
    }

    private boolean checkFields() {
        if (taName.getText().isEmpty()){
            taName.setStyle("-fx-border-color: #ff0000; -fx-border-width: 2px; -fx-border-style: solid;");
            return false;
        }else
            taName.setStyle("");
        if (comboType.getValue() == null)
            return false;
        if (dpDate.getValue() == null)
            return false;
        if (comboTime.getValue() == null)
            return false;
        if (comboSala.getValue() == null)
            return false;
        if (comboDuration.getValue() == null)
            return false;
        if (cbAdmin.isSelected() && tfResponsible.getText().isEmpty()) {
            tfResponsible.setStyle("-fx-border-color: #ff0000; -fx-border-width: 2px; -fx-border-style: solid;");
            return false;
        }
        else
            tfResponsible.setStyle("");
        return true;
    }
}
