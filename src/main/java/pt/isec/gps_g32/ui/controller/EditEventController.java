package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.User;
import pt.isec.gps_g32.model.data.user.UserType;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.ui.IPC4You;
import pt.isec.gps_g32.model.data.state.State;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class EditEventController extends Controller {
    boolean changes;

    @FXML
    Text home2;

    @FXML
    VBox create;

    @FXML
    VBox attendance;

    @FXML
    VBox history;

    @FXML
    VBox home;

    @FXML
    VBox account;

    @FXML
    StackPane stackPane;

    @FXML
    ImageView eventImage;

    @FXML
    TextArea eventName;

    @FXML
    ComboBox eventType;

    @FXML
    DatePicker eventDate;

    @FXML
    ComboBox eventHour;

    @FXML
    ComboBox comboDuration;

    @FXML
    ComboBox eventRoom;

    @FXML
    TextField eventLink;

    @FXML
    TextArea eventNotes;

    @FXML
    ComboBox eventOwner;

    @FXML
    CheckBox eventAdmin;

    @FXML
    CheckBox eventVisibility;

    @FXML
    CheckBox informatica;

    @FXML
    CheckBox egi;

    @FXML
    CheckBox bioengenharia;

    @FXML
    CheckBox mecanica;

    @FXML
    CheckBox civil;

    @FXML
    CheckBox eletromecanica;

    @FXML
    CheckBox engenharia;

    @FXML
    CheckBox softskills;

    @FXML
    CheckBox licenciaturas;

    @FXML
    CheckBox mestrados;

    @FXML
    CheckBox ctesp;

    @FXML
    CheckBox associativismo;

    @FXML
    private Map<String, CheckBox> checkBoxMap = new HashMap<>();

    private Event event;

    private String filename;

    @FXML
    public void initialize() {
        changes = false;
        filename = null;
        eventType.getItems().addAll("Palestra", "Seminário", "Workshop", "Conferência", "Debate");

        for (int hours = 0; hours < 24; hours++) {
            for (int minutes = 0; minutes < 60; minutes += 30) {
                String formattedTime = String.format("%02d:%02d", hours, minutes);
                eventHour.getItems().add(formattedTime);
            }
        }

        for (int hours = 30; hours <= 120; hours+=30) {
            comboDuration.getItems().add(String.valueOf(hours));
        }

        eventRoom.getItems().addAll("ISEC", "ESEC");

        checkBoxMap.put("informatica", informatica);
        checkBoxMap.put("egi", egi);
        checkBoxMap.put("bioengenharia", bioengenharia);
        checkBoxMap.put("mecanica", mecanica);
        checkBoxMap.put("civil", civil);
        checkBoxMap.put("eletromecanica", eletromecanica);
        checkBoxMap.put("engenharia", engenharia);
        checkBoxMap.put("softskills", softskills);
        checkBoxMap.put("licenciaturas", licenciaturas);
        checkBoxMap.put("mestrados", mestrados);
        checkBoxMap.put("ctesp", ctesp);
        checkBoxMap.put("associativismo", associativismo);
    }

    @FXML
    public void checkChanges() {
        changes = true;
    }

    public void setModel(ModelManager model) {
        this.model = model;
        for (User user : model.getUsers()) eventOwner.getItems().add(user.getName());

        registerHandlers();
        update();
    }

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
        });
        //update();
    }

    public void update(){
        boolean visible = (model != null && model.getState() == State.EDIT_EVENT);
        stackPane.setVisible(visible);
        if(visible) {
            _update();
            System.out.println("Edit event");
        }
    }

    private void _update() {
        if(model.getLoggedUser() == null || model.getLoggedUser().getUserType() != UserType.ADMIN) {
            stackPane.setVisible(false);
            Alert confirmation;
            confirmation = new Alert(Alert.AlertType.ERROR);
            confirmation.setTitle("ERRO");
            confirmation.setHeaderText("Não tem permissões para aceder a este conteúdo!");
            confirmation.showAndWait();
            model.changeState(State.MENU);
        }

        try {
            event = model.getActiveEvent();
            this.changes = false;
            Image eventImg = new Image(Objects.requireNonNull(IPC4You.class.getResource("/pt/isec/gps_g32/images/" + event.getImagePath())).toExternalForm());
            eventImage.setImage(eventImg);

            eventName.setText(event.getName());
            eventType.getSelectionModel().select(event.getType());

            String dateStr = event.getDate();

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date = LocalDate.parse(dateStr, dateFormatter);
            eventDate.setValue(date);

            eventHour.getSelectionModel().select(event.getHour());
            eventRoom.getSelectionModel().select(event.getLocal());

            comboDuration.getSelectionModel().select(event.getDurationInMinutes().toString());

            eventLink.setText(event.getLink() != null ? event.getLink() : "");

            for (CheckBox checkBox : checkBoxMap.values()) {
                checkBox.setSelected(false);
            }

            String interestsString = event.getInterests();
            String[] interestsArray = interestsString.split(";");

            for (String interest : interestsArray) {
                CheckBox checkBox = checkBoxMap.get(interest.toLowerCase());
                checkBox.setSelected(true);
            }
            eventNotes.setText(event.getDescription());

            for (User user : model.getUsers()) {
                if (user.getEmail().equals(event.getResponsible())) {
                    eventOwner.getSelectionModel().select(user.getName());
                    break;
                }
            }
            eventAdmin.setSelected(event.isResponsibleHasPermissions());
            eventVisibility.setSelected(event.getVisibility());
        }
        catch (Exception e) {
            e.printStackTrace();
            Alert confirmation;
            stackPane.setVisible(false);
            confirmation = new Alert(Alert.AlertType.ERROR);
            confirmation.setTitle("Erro");
            confirmation.setHeaderText("Evento não encontrado!");
            confirmation.setContentText("O evento não foi encontrado.");
            confirmation.showAndWait();
            model.changeState(State.MENU_ADMIN);
        }
    }

    public void goHome(String vboxId) {
        if(!changes)  {
            switchView(vboxId);
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Sair sem guardar");
            alert.setHeaderText("Tem a certeza que pretende sair sem guardar as alterações ao evento?");
            alert.setContentText("As alterações não serão guardadas.");
            ButtonType buttonTypeOne = new ButtonType("Sim");
            ButtonType buttonTypeTwo = new ButtonType("Não");

            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

            alert.showAndWait().ifPresent(type -> {
                if (type == buttonTypeOne) {
                    switchView(vboxId);
                } else if (type == buttonTypeTwo) {
                    alert.close();
                }
            });
        }
    }

    public void getButtonPressed(MouseEvent event) {
        Object source = event.getSource();
        String vboxId = null;

        if (source instanceof VBox) {
            VBox clickedVBox = (VBox) source;
            vboxId = clickedVBox.getId();
        }
        else if(source instanceof Text) {
            Text clickedText = (Text) source;
            vboxId = clickedText.getId();
        }

        goHome(vboxId);
    }

    private void switchView(String vboxId) {
        switch(vboxId) {
            case "home":
            case "home2":
                model.changeState(State.MENU_ADMIN);
                break;
            case "history":
                model.changeState(State.HISTORY);
                break;
            case "attendance":
                model.changeState(State.ATTENDANCE);
                break;
            case "create":
                if(model.getLoggedIsAdmin()) model.changeState(State.CREATE_EVENT);
                else model.changeState(State.CREATE_EVENT_REQUEST);
                break;
            case "account":
                model.changeState(State.ACCOUNT_INFO);
                break;
        }
    }

    public void confirmationPopup(MouseEvent mouseEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmação");
        alert.setHeaderText("Tem a certeza que pretende guardar as alterações ao evento?");
        alert.setContentText("As alterações serão guardadas e o evento será atualizado.");

        ButtonType buttonTypeOne = new ButtonType("Sim");
        ButtonType buttonTypeTwo = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

        alert.showAndWait().ifPresent(type -> {
            if (type == buttonTypeOne) {
                Alert confirmation;
                if(editEvent()) {
                    confirmation = new Alert(Alert.AlertType.INFORMATION);
                    confirmation.setTitle("Sucesso");
                    confirmation.setHeaderText("Alterações guardadas com sucesso!");
                    confirmation.setContentText("As alterações foram guardadas e o evento foi atualizado com sucesso.");
                    confirmation.showAndWait();
                    alert.close();
                    model.changeState(State.MENU_ADMIN);
                }
                else {
                    confirmation = new Alert(Alert.AlertType.ERROR);
                    confirmation.setTitle("Erro");
                    confirmation.setHeaderText("Alterações não guardadas!");
                    confirmation.setContentText("Existe um ou mais campo(s) sem preenchimento ou com informação inválida.");
                    confirmation.showAndWait();
                }
            } else if (type == buttonTypeTwo) {
                alert.close();
            }
        });
    }

    protected boolean editEvent() {
        try {
            event.setName(eventName.getText());
            event.setType(eventType.getSelectionModel().getSelectedItem().toString());

            LocalDate date = eventDate.getValue();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = date.format(formatter);
            event.setDate(formattedDate);

            event.setHour(eventHour.getSelectionModel().getSelectedItem().toString());
            event.setDurationInMinutes(Integer.parseInt(comboDuration.getSelectionModel().getSelectedItem().toString()));

            StringBuilder interestsStringBuilder = new StringBuilder();

            for (Map.Entry<String, CheckBox> entry : checkBoxMap.entrySet()) {
                String checkboxName = entry.getKey();
                CheckBox checkBox = entry.getValue();

                if (checkBox.isSelected()) {
                    if (!interestsStringBuilder.isEmpty()) {
                        interestsStringBuilder.append(";");
                    }
                    interestsStringBuilder.append(checkboxName);
                }
            }

            event.setInterests(interestsStringBuilder.toString());
            String responsibleEmail = eventOwner.getSelectionModel().getSelectedItem().toString();
            for (User user : model.getUsers()) {
                if (user.getName().equals(responsibleEmail)) {
                    event.setResponsible(user.getEmail());
                    break;
                }
            }
            event.setResponsibleHasPermissions(eventAdmin.isSelected());
            event.setDescription(eventNotes.getText());
            event.setVisibility(eventVisibility.isSelected());
            event.setLocal(eventRoom.getSelectionModel().getSelectedItem().toString());
            event.setLink(eventLink.getText());
            //participants are null for now
            event.setImagePath(filename == null ? event.getImagePath() : filename);

            return model.saveEvents();
        }
        catch (Exception e) {
            return false;
        }
    }

    public void filePopup() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select an image file");

        String userDir = System.getProperty("user.dir");
        Path imagesPath = Paths.get(userDir, "src", "main", "resources", "pt", "isec", "gps_g32", "images");
        fileChooser.setInitialDirectory(imagesPath.toFile());

        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif", "*.bmp"));

        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null) {
            String fileName = selectedFile.getName();
            URL imageURL = IPC4You.class.getResource("/pt/isec/gps_g32/images/" + fileName);

            assert imageURL != null;
            eventImage.setImage(new Image(imageURL.toExternalForm()));
            checkChanges();
        }
    }

    public void deletePopup(MouseEvent mouseEvent) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Confirmação");
        alert.setHeaderText("Tem a certeza que pretende eliminar o evento?");
        alert.setContentText("O evento será eliminado permanentemente.");

        ButtonType buttonTypeOne = new ButtonType("Sim");
        ButtonType buttonTypeTwo = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

        alert.showAndWait().ifPresent(type -> {
            if (type == buttonTypeOne) {
                Alert confirmation;
                if(deleteEvent()) {
                    confirmation = new Alert(Alert.AlertType.INFORMATION);
                    confirmation.setTitle("Sucesso");
                    confirmation.setHeaderText("Evento eliminado com sucesso!");
                    confirmation.setContentText("O evento foi eliminado com sucesso.");
                    confirmation.showAndWait();
                    alert.close();
                    model.changeState(State.MENU_ADMIN);
                }
                else {
                    confirmation = new Alert(Alert.AlertType.ERROR);
                    confirmation.setTitle("Erro");
                    confirmation.setHeaderText("Evento não eliminado!");
                    confirmation.setContentText("O evento não foi eliminado.");
                    confirmation.showAndWait();
                }
            } else if (type == buttonTypeTwo) {
                alert.close();
            }
        });
    }

    protected boolean deleteEvent() {
        return model.deleteEvent();
    }
}