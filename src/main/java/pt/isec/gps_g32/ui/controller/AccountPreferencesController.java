package pt.isec.gps_g32.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.state.State;
import pt.isec.gps_g32.model.data.user.Interest;
import java.util.ArrayList;
import java.util.Arrays;

public class AccountPreferencesController extends Controller{

    @FXML
    StackPane stackPane;

    @FXML
    CheckBox cbAssociativismo,cbInformatica, cbEGI, cbBioengenharia, cbMecanica, cbCivil, cbEletromecanica, cbEngenharia, cbSoftSkills, cbLicenciaturas, cbMestrados, cbCTESP;

    public void registerHandlers() {
        model.addPropertyChangeListener(ModelManager.PROP_STATE, evt -> {
            update();
            prepare();
        });
        update();
    }

    private void prepare() {
        if (model.getState() == State.ACCOUNT_INFO) {
            model.getLoggedUser().getInterests().forEach(it -> {
                switch (it) {
                    case INFORMATICA:
                        cbInformatica.setSelected(true);
                        break;
                    case EGI:
                        cbEGI.setSelected(true);
                        break;
                    case BIOENG:
                        cbBioengenharia.setSelected(true);
                        break;
                    case MECANICA:
                        //cbMecanica.setSelected(true);
                        break;
                    case CIVIL:
                        cbCivil.setSelected(true);
                        break;
                    case ELETROMECANICA:
                        cbEletromecanica.setSelected(true);
                        break;
                    case ENG:
                        cbEngenharia.setSelected(true);
                        break;
                    case SOFT_SKILLS:
                        cbSoftSkills.setSelected(true);
                        break;
                    case LICENCIATURAS:
                        cbLicenciaturas.setSelected(true);
                        break;
                    case MESTRADOS:
                        cbMestrados.setSelected(true);
                        break;
                    case CTESP:
                        cbCTESP.setSelected(true);
                        break;
                }
            });
        }
    }

    @Override
    protected void update() {
        this.stackPane.setVisible(model != null && model.getState() == State.ACCOUNT_INFO);
    }

    public void handleSave(MouseEvent mouseEvent) {
        ArrayList<Interest> interests = new ArrayList<>();
        if (cbInformatica.isSelected()) {
            interests.add(Interest.INFORMATICA);
        }
        if (cbEGI.isSelected()) {
            interests.add(Interest.EGI);
        }
        if (cbBioengenharia.isSelected()) {
            interests.add(Interest.BIOENG);
        }
        if (cbMecanica.isSelected()) {
            interests.add(Interest.MECANICA);
        }
        if (cbCivil.isSelected()) {
            interests.add(Interest.CIVIL);
        }
        if (cbEletromecanica.isSelected()) {
            interests.add(Interest.ELETROMECANICA);
        }
        if (cbEngenharia.isSelected()) {
            interests.add(Interest.ENG);
        }
        if (cbSoftSkills.isSelected()) {
            interests.add(Interest.SOFT_SKILLS);
        }
        if (cbLicenciaturas.isSelected()) {
            interests.add(Interest.LICENCIATURAS);
        }
        if (cbMestrados.isSelected()) {
            interests.add(Interest.MESTRADOS);
        }
        if (cbCTESP.isSelected()) {
            interests.add(Interest.CTESP);
        }
        if(cbAssociativismo.isSelected()){
            interests.add(Interest.ASSOCIATIVISMO);
        }
        System.out.println(Arrays.toString(interests.toArray()));
        model.saveUserInterests(interests);
        Alert saveAlert;
        saveAlert = new Alert(Alert.AlertType.CONFIRMATION);
        saveAlert.setTitle("Dados guardados!");
        saveAlert.setHeaderText("As suas alterações foram guardadas");
        saveAlert.showAndWait();
    }

    public void handleClear(MouseEvent mouseEvent) {
        //Unselect all cbs
        cbInformatica.setSelected(false);
        cbEGI.setSelected(false);
        cbBioengenharia.setSelected(false);
        cbMecanica.setSelected(false);
        cbCivil.setSelected(false);
        cbEletromecanica.setSelected(false);
        cbEngenharia.setSelected(false);
        cbSoftSkills.setSelected(false);
        cbLicenciaturas.setSelected(false);
        cbMestrados.setSelected(false);
        cbCTESP.setSelected(false);
        cbAssociativismo.setSelected(false);

    }


}
