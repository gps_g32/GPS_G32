package pt.isec.gps_g32.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.Data;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.User;
import pt.isec.gps_g32.ui.controller.Controller;

import java.io.IOException;

public class IPC4You extends Application {
    @Override
    public void init() throws Exception {
        super.init();

        //TODO: listagem da "base de dados" para testes
        System.out.println("-----DataBase-----");
        Data data = new Data();

        System.out.println("Users:\n");
        for (User u : data.getUsers())
            System.out.println(u);

        System.out.println("\nEvents:\n");
        for (Event e : data.getEvents()) {
            System.out.println(e);
        }
    }

    @Override
    public void start(Stage stage) throws IOException {
        ModelManager model = new ModelManager();
        FXMLLoader fxmlLoader = new FXMLLoader(IPC4You.class.getResource("root-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 350, 600);
        RootViewController controller = fxmlLoader.getController();
        controller.setModel(model);
        stage.setOnCloseRequest(e -> {
            model.save();
        });
        stage.setTitle("IPC4You");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
}