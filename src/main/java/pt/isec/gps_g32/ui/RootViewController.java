package pt.isec.gps_g32.ui;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.fxml.FXMLLoader;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.ui.controller.*;

import java.io.IOException;

public class RootViewController {
    private ModelManager model;
    @FXML private StackPane stackPane;

    @FXML
    private void initialize(){
    }

    public void setModel (ModelManager model) throws IOException {
        this.model = model;
        createViews();
    }

    private void createViews() throws IOException {
        loadView("login-view.fxml");
        loadView("list-events-view.fxml");
        loadView("list-events-view_admin.fxml");
        loadView("event-history-view.fxml");
        loadView("create-event-view.fxml");
        loadView("edit-event-view.fxml");
        loadView("request-event-view.fxml");
        loadView("account-preferences-view.fxml");
        loadView("account-preferences_admin-view.fxml");
        loadView("list-event-requests-view_admin.fxml");
        loadView("select-event-view.fxml");
        loadView("select-option-view_admin.fxml");
        loadView("event-history-view_admin.fxml");
        loadView("user-history-view_admin.fxml");
    }

    private void loadView(String fxmlPath) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(IPC4You.class.getResource(fxmlPath));
        Parent child = fxmlLoader.load();
        Controller controller = fxmlLoader.getController();
        controller.setModel(this.model);
        controller.registerHandlers();
        stackPane.getChildren().add(child);
    }
}
