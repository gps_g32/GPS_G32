package pt.isec.gps_g32;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.isec.gps_g32.model.ModelManager;

import static org.junit.jupiter.api.Assertions.*;

class ParticipationTest {
    private ModelManager model;

    @BeforeEach
    void setUp() {
        model = new ModelManager();
        model.createUser("model@isec.pt");
        model.setLoggerUser("model@isec.pt");
    }

    @Test
    void participation() {
        if(model.checkEventExists(1)) {
            if(model.isParticipating(1)) {
                model.removeParticipation(1);
                assertFalse(model.isParticipating(1));
            }
            else {
                model.addParticipation(1);
                assertTrue(model.isParticipating(1));
            }
            assertFalse(model.isParticipating(99999));
        }
    }
}
