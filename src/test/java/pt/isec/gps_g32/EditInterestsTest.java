package pt.isec.gps_g32;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.user.Interest;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class EditInterestsTest {

    private ModelManager modelManager;

    @BeforeEach
    public void setUp() {
        modelManager = new ModelManager();
    }

    @Test
    public void testRemoveInterests() {
        modelManager.authenticate("tiago@isec.pt", "12345");
        modelManager.getLoggedUser().getInterests().clear();
        modelManager.save();
        modelManager = new ModelManager();
        modelManager.setLoggerUser("tiago@isec.pt");
        assertNotEquals( false, modelManager.getLoggedUser().getInterests().isEmpty());
        assertEquals(0, modelManager.getLoggedUser().getInterests().size());
    }

    @Test
    public void testAddInterests() {
        modelManager.authenticate("tiago@isec.pt", "12345");
        ArrayList<Interest> interests = new ArrayList<>();
        interests.add(Interest.ASSOCIATIVISMO);
        interests.add(Interest.INFORMATICA);
        interests.add(Interest.CIVIL);
        modelManager.getLoggedUser().setInterests(interests);
        modelManager.save();
        modelManager = new ModelManager();
        modelManager.setLoggerUser("tiago@isec.pt");
        assertEquals(interests, modelManager.getLoggedUser().getInterests());
    }
}
