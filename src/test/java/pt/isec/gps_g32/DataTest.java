package pt.isec.gps_g32;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.isec.gps_g32.model.data.Data;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.Interest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
class DataTest {

    Data data;
    @BeforeEach
    public void init(){
        data = new Data();
    }

    @Test
    void createEvent() {
        Event eventTest = new Event(9999, "Test Event", "Palestra", "2023-01-01", "15:00", new ArrayList<>(),
                "tiago@isec.pt", true, "whatever", true, "Sala 1", "link.pt",
                new ArrayList<>(), "evento3.png", 60, 0.0, new ArrayList<>());
        Event eventTest1 = new Event(9999, "Test Event", "Palestra", "2023-01-01", "15:00", new ArrayList<>(),
                "tiago@isec.pt", true, "whatever", true, "Sala 1", "link.pt",
                new ArrayList<>(), "evento3.png", 60, 0.0, new ArrayList<>());
        eventTest.addInterest(Interest.ENGENHARIA);
        eventTest1.addInterest(Interest.ENGENHARIA);

        assertFalse(data.createEvent(eventTest1));
    }


    @Test
    void createEventRequest() {
        Event eventTest = new Event(9999, "Test Event", "Palestra", "2023-01-01", "15:00", new ArrayList<>(),
                "tiago@isec.pt", true, "whatever", true, "Sala 1", "link.pt",
                new ArrayList<>(), "evento3.png", 60, 0.0, new ArrayList<>());
        eventTest.addInterest(Interest.ENGENHARIA);

        //Not logged id
        assertFalse(data.createEventRequest(eventTest));
        data.setLoggerUser("admin@isec.pt");
        //Amin
        assertFalse(data.createEventRequest(eventTest));

        //User
        data.setLoggerUser("tiago@isec.pt");
        assertTrue(data.createEventRequest(eventTest));


    }

    @Test
    void getEventHistory() {
        assertNull(data.getEventHistory());

        data.authenticate("tiago@isec.pt", "12345");
        List<Event> eventsTest = data.getEventHistory();

        assertNotNull(eventsTest);
    }

    @Test
    void processEventRequest() {
        Event eventTest = new Event(-5, "Test Event", "Palestra", "2023-01-01", "15:00", new ArrayList<>(),
                "tiago@isec.pt", true, "whatever", true, "Sala 1", "link.pt",
                new ArrayList<>(), "evento3.png", 60, 0.0, new ArrayList<>());
        eventTest.addInterest(Interest.ENGENHARIA);

        //Not logged id
        assertFalse(data.processEventRequest(-5, false, false));

        //User
        data.setLoggerUser("tiago@isec.pt");
        data.createEventRequest(eventTest);
        assertFalse(data.processEventRequest(-5, false, false));

        //ID does not exist
        data.setLoggerUser("admin@isec.pt");
        assertFalse(data.processEventRequest(-1, false, false));

        //Admin logged and Event exists
        assertTrue(data.processEventRequest(-5, false, false));
    }
}