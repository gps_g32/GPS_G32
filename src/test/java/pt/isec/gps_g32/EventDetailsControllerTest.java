package pt.isec.gps_g32;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.UserRatingEvent;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class EventDetailsControllerTest {
    private ModelManager model;
    private Event e;

    @BeforeEach
    void setUp() {
        model = new ModelManager();
        //model.createEventsFile();
        model.setLoggerUser("admin@isec.pt");
        e = new Event(9999, "Test Event", "Palestra", "2023-01-01", "15:00", new ArrayList<>(),
                "tiago@isec.pt", true, "whatever", true, "Sala 1", "link.pt",
                new ArrayList<>(), "evento3.png", 60, 0.0, new ArrayList<>());
    }

    @Test
    void addRating() {
        assertEquals(e.getRatingList().size(), 0);
        e.addRating("user", 5.0);
        assertEquals(e.getRatingList().size(), 1);
        e.addRating("user", 3.0);
        e.addRating("cliente", 1.0);
        assertEquals(e.getRatingList().size(), 2);
    }

    @Test
    void addRatingUser() {
        assertNull(e.getUserRating("user"));
        e.addRating("user", 5.0);
        assertNotNull(e.getUserRating("user"));

    }

    @Test
    void saveRating() {
        assertEquals(e.getRating(), 0.0);
        e.addRating("user", 5.0);
        assertEquals(e.getRating(), 5.0);
        e.addRating("user", 3.0);
        assertEquals(e.getRating(), 3.0);
        e.addRating("cliente", 1.0);
        assertEquals(e.getRating(), 2.0);
    }
}