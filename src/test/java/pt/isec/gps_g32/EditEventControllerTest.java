package pt.isec.gps_g32;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.event.Event;
import pt.isec.gps_g32.model.data.user.UserType;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class EditEventControllerTest {
    private ModelManager model;

    @BeforeEach
    void setUp() {
        model = new ModelManager();
        model.createUser("model@isec.pt");
        model.setLoggerUser("admin@isec.pt");
    }

    @Test
    void saveEvent() {
        assertTrue(model.saveEvents());
    }

    @Test
    void deleteEventValid() {
        //ID exists
        if(!model.checkEventExists(9999)) {
            Event eventTest = new Event(9999, "Test Event", "Palestra", "2023-01-01", "15:00", new ArrayList<>(),
                    "tiago@isec.pt", true, "whatever", true, "Sala 1", "link.pt",
                    new ArrayList<>(), "evento3.png", 60, 0.0, new ArrayList<>());
            model.createEvent(eventTest);
        }
        model.clickedEvent(9999);
        assertTrue(model.deleteEvent()); //this functions returns if save was successful, not if the event was deleted
        assertNull(model.getActiveEvent());
        //not admin
        model.setLoggerUser("normie@isec.pt");
        assertFalse(model.deleteEvent()); //should return false because the user is not admin
    }

    @Test
    void deleteEventInvalid() {
        //Active Event ID does not exist
        model.clickedEvent(99999);
        assertFalse(model.deleteEvent()); //return false because the event does not exist
        assertNull(model.getActiveEvent());
        //not admin
        model.setLoggerUser("model@isec.pt");
        assertFalse(model.deleteEvent()); //should return false because the user is not admin
        assertNull(model.getActiveEvent());
    }
}