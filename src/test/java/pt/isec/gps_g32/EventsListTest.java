package pt.isec.gps_g32;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import pt.isec.gps_g32.model.ModelManager;
import pt.isec.gps_g32.model.data.user.Filter;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class EventsListTest {

    @Test
    public void getEvents(){
        ModelManager model = new ModelManager();
        model.authenticate("tiago@isec.pt", "12345");
        model.setCurrentEventFilter(Filter.ALL_EVENTS);
        assertNotEquals(null, model.getEvents());
        model.setCurrentEventFilter(Filter.INTERESTED);
        assertNotEquals(0,model.getEvents().size());
    }
}