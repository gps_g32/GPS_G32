package pt.isec.gps_g32;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.isec.gps_g32.model.ModelManager;

import static org.junit.jupiter.api.Assertions.*;

class FavouritesTest {
    private ModelManager model;

    @BeforeEach
    void setUp() {
        model = new ModelManager();
        model.createUser("model@isec.pt");
        model.setLoggerUser("model@isec.pt");
    }

    @Test
    void checkFavouriteStatus() {
        assertFalse(model.getFavourite(999999)); //event doesn't exist, should return false
        model.addFavourite(2);
        assertTrue(model.getFavourite(2)); //event exists, should return true (since this user has it as favourite)
        model.removeFavourite(2);
        assertFalse(model.getFavourite(2)); //event exists, should return false (since this user doesn't have it as favourite)
    }
}
