# IPC4YOU

## Contents

- [Team](#team)
- [Vision and Scope](#vision-and-scope)
- [Requirements](#requirements)
    - [Use case diagram](#use-case-diagram)
    - [Mockups](#mockups)
    - [User stories](#user-stories)
- [Definition of Done](#definition-of-done)
- [Architecture and Design](#architecture-and-design)
    - [Domain Model](#domain-model)
- [Risk Plan](#risk-plan)
- [Pre-Game](#pre-game)
- [Release Plan](#release-plan)
  - [Release 1](#release-1)
  - [Release 2](#release-2)
- [Increments](#increments)
  - [Sprint 1](#sprint-1)
  - [Sprint 2](#sprint-2)
  - [Sprint 3](#sprint-3)

## Team

- Bruno Trindade - 2019132612
- Guilherme Camacho - 2021138502
- João Gomes - 2019126992
- Tiago Correia - 2020127677
- Tiago Figueiredo - 2020122664

***

## Vision and Scope

#### Problem Statement

##### Project background
  
IPC4YOU is a project that aims to solve the problem of the lack of attendance at events organised by IPC.

As IPC students, we are bombarded daily with emails on a wide variety of subjects, including notices, events, advertisements, job offers, etc... 
The fact that most of these emails are of little interest to us, due to their daily quantity, means that they are automatically discarded and, as a result, many of the events that might be of interest to us Computer Engineering students are easily ignored.
Most of these students have attended 1/2 lectures in 3 years, which is relatively little considering the amount of free time we have allocated to this type of event.
Our idea is to develop an application that lists all the events that will be held at/by IPC. This app should inform students via a pop-up notification on their smartphone according to their tastes/preferred types of events/themes.

With this project, students and teachers will have an application where they can see events that interest them without having to scroll through endless events that are not in the users interest.
There will be no more visual trash and can make the users actually attend the events. Also, as a result of this application, the efficiency of IPC's email will significantly improve, as it will keep the essential messages and remove the unnecessary emails related to these events.


##### Stakeholders

- IPC's students
- IPC's teachers
- Offices and departments of IPC

##### Users

- Students of IPC
- Admins that are in charge of sending the emails.
- Teachers of IPC

***

#### Vision & Scope of the Solution

##### Vision statement

Our vision is to create a versatile application that will serve as a platform for students, teachers, and administrators at IPC. 
We aim to enrich the overall academic experience by facilitating more efficient communication between the university and its students, providing easy access to all scheduled lectures and workshops.
Our application offers a customisable experience, allowing users to adapt it to their individual needs and interests.
Whether you are a student seeking new learning opportunities or a teacher/administrator aiming to enhance engagement during these events, this platform will be the solution you need.

##### List of features

As our main solution for the problem stated above is simplifying the event management our features will focus on exactly on that.

Some of the features will  be:
- A way for users to filter the events that interest them.
- Enable the users to get email notifications about events that interest them.
- Enable the users to get events to appear automatically in their schedule, that is if the event does not happen during a class that the student needs to attend.
- When a new event is launched on the app, an email and a push notification is sent to everyone who has the respective subject selected.
- History of the events that they have attended in the past.
- Simple listing of the events. Title of the event, what will be addressed in it (sort of like a synopsis) and the date and time of the event. This way the users can get the important information given to them without any clutter.
- A way of sharing events by a link.

##### Features that will not be developed

- The user can select which session to attend so that the organisers can drop sessions or not.
- Connection with the teacher's syllabus - this would consist of alerting the teacher to a matching event on the material they are teaching.
- Check capacity with registrants - if the number of registrants exceeds 150% of the room's capacity, the app must notify the organiser so that they can try to book another room for the event.
- Admins can create a poll as a way to find out what events the users are interested of attending to. For example, for CS users to vote if they want events about programming, web security, web development, etc...
- Admins can create a poll inquiring about possible event times so that the majority of the users that want to attend have the availability to do so.
- Know if an event is about to take in place of a lecture. For example, if a lecture is cancelled and an event is taking its place, the users will be notified of this change.
- A possibility to create inboxes. For example, a user can have 2 inboxes, let's call them A and B, and for each inbox they can have a set of filters. Inbox A is all about events surrounding java and OOP languages and inbox B is all about low level languages like C.


##### Assumptions

- User Interest: The project assumes that there is a genuine interest among students and teachers in attending events at IPC once they are better informed. It assumes that the lack of attendance is primarily due to the information overload and not a lack of interest in events.
- Administrative Support: The project assumes that the university administration will support and actively participate in the application's features.
- User Notification Preferences: The project assumes that users will want to receive notifications about events via email and push notifications, and that they will appreciate this form of communication.
- Event Scheduling: It assumes that the university can effectively schedule events to minimize conflicts with classes and that event information will be consistently updated.
- User Adoption: Whether the students and teachers will adopt and actively use the application. If they don't see the value or find it inconvenient to use, the project's success will be compromised.
- Data Security: Handling user preferences and personal information for notifications and scheduling introduces data security concerns. A data breach could have serious consequences.
- User Privacy: Users may have concerns about their data privacy, especially if the application collects and uses personal information for customisation.
- User Authentication: All authentication and person-related information is provided by the InforEstudante - IPC platform database.

***

## Requirements

#### Use Case Diagram

![Use case diagram](imgs/UseCaseDiagram.jpg)

***

### Mockups
###### User Story 1 - Login
![User Story 1 - Login](Prototypes/examples/Login.png)
###### User Story 2 - Forgot Password
![User Story 2 - Forgot Password](Prototypes/examples/Login_w_chars.png)
###### User Story 3 - List Events
![User Story 3 - List Events](Prototypes/examples/List_Events.png)
###### User Story 4 - Change Account Preferences
![User Story 4 - Change Account Preferences](Prototypes/examples/Account.png)
###### User Story 5 - Create Event (Screen 1 and 2)
![User Story 5 - Create Event 1](Prototypes/examples/Criar1.png)
![User Story 5 - Create Event 2](Prototypes/examples/Criar2.png)
###### User Story 6 - Edit Events , User Story 7 - Delete Event, User story 8 - Change Visibility (Screens 1 and 2)
![User Story 6,7,8 - Edit Events 1](Prototypes/examples/Edit_Event1.png)
![User Story 6,7,8 - Edit Events 2](Prototypes/examples/Edit_Event2.png)
###### User Story 9 - Validate Attendance
![User Story 9 - Validate attendance](Prototypes/examples/ValidateAttendance.png)
###### User Story 10 - Show Interest
![User Story 10 - Show Interest in Event](Prototypes/examples/Mark_Event.png)
###### User Story 11 - Request Event
![User Story 11 - Request Event 1](Prototypes/examples/Request1.png)
![User Story 11 - Request Event 2](Prototypes/examples/Request2.png)
###### User Story 12 - List Event History
![User Story 12 - List Event History](Prototypes/examples/History.png)
###### User Story 13 - Recover Password
![User Story 13 - Recover Password](Prototypes/examples/RecoverPassword.png)
###### User Story 14 - List Favourite Events
###### User Story 15 - Download Attendance Certificate
![User Story 15 - Download Attendance Certificate](Prototypes/examples/History.png)



***

### User Stories

- User Story 1 - Login (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/3)
- User Story 2 - Forgot Password (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/4)
- User Story 3 - List Events (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/5)
- User Story 4 - Change Account Preferences (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/6)
- User Story 5 - Create Event (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/8)
- User Story 6 - Edit Event (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/40)
- User Story 7 - Delete Event (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/7)
- User Story 8 - Change Visibility (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/9)
- User Story 9 - Validate Attendance (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/11)
- User Story 10 - Show Interest (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/12)
- User Story 11 - Request Event (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/13)
- User Story 12 - List Event History (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/210)
- User Story 13 - Recover Password (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/188)
- User Story 14 - List Favourite Events (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/198)
- User Story 15 - Download Attendance Certificate (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/10)
- User Story 16 - Search Events (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/228)
- User Story 17 - List Event's Attendance (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/229)
- User Story 18 - List User's Events Attendance (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/230)
- User Story 19 - List User's Favourite Events (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/231)
- User Story 20 - Accept Event Requests (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/233)
- User Story 21 - Rate Events (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/234)
- User Story 22 - List Live Events (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/235)
- User Story 23 - Send Recovery Code on Email (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/236)
- User Story 24 - List Event's User Favourites (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/237)


***

## Definition of done

(This section is already written, do not edit)
It is a collection of criteria that must be completed for a User Story to be considered “done.”

1. All tasks done:
- CI – built, tested (Junit), reviewed (SonarCloud)
- Merge request to qa (code review)
2. Acceptance tests passed
3. Accepted by the client
4. Code merged to main

***

## Architecture and Design

#### Domain Model
![Use case diagram](imgs/DomainModel.jpg)
***

### Risk plan

##### Threshold of Success
- By the first sprint, we want to give the client a preview of the app's user interface.
- By the second sprint, the most important features are closed.
- By the third sprint, we want to have a usable and fully functional app to display ISEC's events according to users interests.

##### Risk List
- RSK1 – PxI: 2*2=4; The team is not used to work together so the communications might not be the best
- RSK2 – PxI: 2*3=6; It's the first time planning and developing a project like this, so we might not be able to manage the project properly and the estimations can be far off.
- RSK3 - PxI: 1*3=3; Additional elements or changes requested by the client can cause delays developing the project.
- RSK4 - PxI: 3*2=6; The team has a limited amount of time, so we might not be able to finish all the features we planned.
- RSK5 - PxI: 2*2=4; The project may become too expensive if we fail to estimate the cost of each feature.
- RSK6 - PxI: 1*3=3; The team is without a member due to a general problem at the UI's.
- RSK7 - PxI: 0*1=0; The team misses the weekly face-to-face meetings and there are communication conflicts.

##### Mitigation Actions

- RSK1 – AS; Maintain open and frequent communication between the members so that everyone is heard.
- RSK2 - MS; Frequently check our work and our estimations to verify if we need to review any estimation
- RSK3 - MS; Clear and frequent communication with the client to manage expectations and address any feedback or changes promptly.
- RSK4 - MS; Every week, check the progress of the project to have a better estimate of the time required.
- RSK5 - MS: More frequent work integration with all members.
- RSK6 - CP; Every week, the team must meet to keep the programming techniques synchronized. 


***

## Pre-Game
### Sprint 0 Plan

- Goal: description
- Dates: from 10-13/Oct to 24-27/Oct, 2 weeks

- Roles:
  - Product Owner: João Bernardo Marinheiro Gomes
  - Scrum Master: Guilherme Camacho

- Sprint 0 Backlog:
  - Task1 – Write Team
  - Task2 – Write V&S
  - Task3 – Write Requirements
  - Task4 – Write DoD
  - Task5 – Write Architecture&Design
  - Task6 – Write Risk Plan
  - Task7 – Write Pre-Game
  - Task8 – Write Release Plan
  - Task9 – Write Product Increments
  - Task10 – Create Product Board
  - Task11 – Create Sprint 0 Board
  - Task12 – Write US in PB, estimate (SML), prioritize (MoSCoW), sort
  - Task13 – Create repository with “GPS Git” Workflow
  
***

## Release Plan

### Release 1
- Goal: MVP - We define our MVP as an app capable of solving 
the stated problem, that is, an app that manages events, and can 
facilitate the way that the students of IPC have the information about events delivered to them. 
We don't plan to go too much in depth with the features, we want to have the base application and the event management working.

- Date: 21-24/Nov

- Release: V1.0

***

### Release 2

- Goal: Final release - The goal for the finale release is to have the app fully functional and will 
the features that we are planning on implementing as well with the features that our client will ask us to do.

- Date:12-15/Dec

- Release: V2.0

***

## Increments

### Sprint 1
##### Sprint Plan

- Goal: The goal for this sprint is to create a prototype of the app's user interface and to implement the most important features that involve the base solution of the problem, for example the listing of events, the creation of the events.
The app will be a simple final app, an app that could be already shipped out to the users and would accomplish the goal of the project. 


    In a short sentence: The goal is to have the way the users have the information about events delivered to them renovated in a simpler way.


- Dates: from 24-27/Oct to 7-10/Nov, 2 weeks

- Roles:
  - Product Owner: Tiago Figueiredo
  - Scrum Master: Tiago Correia

- To do (Ordered by importance):
  - US1 - As a user, I want to login to the application so that I can access my account.
  - US2 - As a user, I want to be able to recover my password so that I can login.
  - US3 - As a user, I want to see the events that match my interests so that I can attend in them.
  - US5 - As a user (admin), I want to create an event so that the event will be shown to the users.
  - US6 - As a user with permissions (admin, responsible of the event), I want to be able to edit the event information so that it is always up-to-date.
  - US12 - As a user, I want to see the events that have already taken place and so I can download the attendance certificate.

- Story Points: Small: 4, Medium: 1, Large: 1; Total = 4S + 1M + 1L = 4S 1M 1L

- Analysis: As a team we have met and made the decision that in the first sprint we will aim to have a project that has a really solid foundation for the team to work on in the future, we will aim to start solving our problem and then we will begin add features.

##### Sprint Review

- Analysis: 
  - US1: Task 5 (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/107)
  - US2: Task 3 (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/111)
  - US3: Task 4 (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/115)
  - US5: Task 6 (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/128)
  - US6: Task 6 (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/134)
  - US12: Task 3 (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/213)

- Story Points: 0

- Version: 0.1

- Conclusions: Initially, we thought we'd be able to complete all the respective tasks more successfully, but we had some problems structuring the code.

##### Sprint Retrospective

- What we did well:
  - Communication between group members, without any personal/professional knowledge between members, proved to be quite good and effective.
- What we did less well:
  -  Everything worked well until we decided to change the only face-to-face meetings, which lasted as long as the possible hours of individual work, to a remote meeting since the work that could be done could be invidivual and without recourse to attendance.
- How to improve to the next sprint:
  - Return to face-to-face meetings to ensure that everyone is working properly. 
***

#### Sprint 2
##### Sprint Plan

- Goal: The goal of this sprint is to implement a final set of features so that the application is fully usable, even if it doesn't have some features that are considered extra.

    In a short sentence: The goal is to present the fully functional application in a simpler way.

- Dates: from 7-10/Nov to 21-24/Nov, 2 weeks

- Roles:
  - Product Owner: Guilherme Camacho
  - Scrum Master: Bruno Trindade

- To do (Ordered by importance):
  - US13 - As a user, I want to receive a random generated code so that I can change my password account.
  - US14 - As a user, I want to be able to filter which set of events I want to see.
  - US10 - As a user, I would like to set an event as favourite.
  - US7 - As a user (admin), I want to delete a given event so that it does not exist anymore.
  - US9 - As a user, I would like to validate my attendance at the event so that I can later obtain my attendance certificate.
  - US11 - As a user, I want to be able to make an event request.
  - US8 - As a user (admin), I want to change if the event is hidden or not.
  - US4 - As a user, I want to change my account settings to suit me.
 
- Story Points: Small: 5, Medium: 3; Total = 5S + 3M = 5S 3M

- Analysis: We had new user stories requested by the client and created a fix to Sprint 1.

##### Sprint Review

- Analysis:
  - (Added)
  - Document the sprint: (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/173) 
  - Revision of the sprint: (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/172)
  - Setup project backend and setup java fx: (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/187)

- Story Points:
  - SP1: 4S + 1M + 1L = 4S 1M 1L
  - SP2: 5S + 3M = 5S 3M
  - TOTAL: 9S + 4M + 1L

- Version: 0.2

- Client analysis: client feedback

- Conclusions: As a team, we managed to complete all the User Stories we had planned for this Sprint, adding new ones for the next Sprint.

##### Sprint Retrospective

- What we did well:
  - We went back to the meetings. We have corrected our code structure programming procedure. 
- What we did less well:
  - We failed to insert the times in the tasks.
- How to improve to the next sprint:
  - Insert the times in the tasks.
***

### Sprint 3
##### Sprint Plan

- Goal: The goal of this sprint is to finish the application with all the clients requests implemented.
    In a short sentence: The goal is present the fully functional application with some nice to have features.

- Dates: from 21-24/Nov to 12-15/Dec, 3 weeks

- Roles:
  - Product Owner: Tiago Correia
  - Scrum Master: João Gomes

- To do (Ordered by importance):
  - US9: As a user, I would like to mark my attendance in a given event. 
  - US21: As a user, I want to rate an event. 
  - US18: As an admin, I want to list the events watched by a given user. 
  - US17: As an admin, I want to list the user presences for a given event. 
  - US24: As an admin, I want to list users who marked a given event as favorite 
  - US19: As an admin, I want to list the favorite events for a given user. 
  - US16: As a user, I want to search for specific events by name. 
  - US20: As an admin, I want to accept user's requests to create events. 
  - US22: As a user, I want to see occurring events.

- Story Points: Small: 7, Medium: 2, Large: 0; Total = 7S + 2M + 0L = 7S 2M

- Analysis: We are going to finish some important features previously defined, and some new features requested by the client

##### Sprint Review

- Analysis: 
  - (Added)
  - Git Issue (https://gitlab.com/JoaoCapitao/GPS_G32/-/issues/285)

- Story Points: 
  - SP1: 4S + 1M + 1L = 4S 1M 1L
  - SP2: 5S + 3M = 5S 3M
  - SP3: 7S + 1M = 7S 1M
  - TOTAL: 16S + 5M + 1L

- Version: 0.3

- Conclusions: Everything went well, but when we needed to make lots of user stories in different branches, the code merges didn't go as we'd expected.

##### Sprint Retrospective

- What we did well:
  - We completed all the user stories we set to do until the end of the sprint.
- What we did less well:
  -  We should have had a more in-depth knowledge of the Git commands, we would have had the same result but in less time, which could have allowed us to complete the User stories we hadn't finished before.
- How to improve to the next sprint:
  - -

